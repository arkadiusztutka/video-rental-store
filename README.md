# README #

# General information

Video rental store is an application for managing the rental administration. It has three primary functions.
1. Have an inventory of films
2. Calculate the price for rentals
3. Keep track of the customers “bonus” points

## Price
The price of rentals is based type of film rented and how many days the film is rented for. The customers say when renting for how many days they want to rent for and pay up front. If the film is returned late, then rent for the extra days is charged when returning.
 
 
The store has three types of films:  
1. New releases – Price is <premium price> times number of days rented.  
2. Regular films – Price is <basic price> for the first 3 days and then <basic price> times the number of days over 3.  
3. Old film - Price is <basic price> for the first 5 days and then <basic price> times the number of days over 5 <premium price> is 40 SEK <basic price> is 30 SEK  

## Examples of price calculations
Matrix 11 (New release) 1 days 40 SEK  
Spider Man (Regular rental) 5 days 90 SEK  
Spider Man 2 (Regular rental) 2 days 30 SEK  
Out of Africa (Old film) 7 days 90 SEK 
 
 
Total price: 250 SEK  
When returning films late  
Matrix 11 (New release) 2 extra days 80 SEK  
Spider Man (Regular rental) 1 days 30 SEK  
Total late charge: 110 SEK  

## Bonus points
Customers get bonus points when renting films. A new release gives 2 points and other films give one point per rental (regardless of the time rented).


# Used technologies
- Java 9
- Spring boot 2.0
- Spring Data JPA + Hibernate
- Liquibase
- Mockito
- JUnit
- PostgreSQL

# How do I get set up?

1. Please install database PostgreSQL(https://www.postgresql.org/download/) version 10.3. Configure and create database.
2. Please find in project two files applications.properties and liquibase.properties and set properties mark with comments.
3. When properties for both liquibase and application will be set application can be run with commend: mvn spring-boot:run

# Additional information

1. Only clients that give email(account for them will be created) can rent films.
I made this requirement due to gather bonus points by every client.  
2. Simplified class Client and Film:
Class Client has only three properties(email, firstName, lastName) because mostly I wanted it to be simple and dont overdo.
Class Film has only two properties(title, type) also because of simplifying this entity but with ease can be extended.

