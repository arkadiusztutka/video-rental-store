package com.casumo.videorentalstore.datetime;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrentDateTimeServiceImplTest
{
   private CurrentDateTimeServiceImpl service = new CurrentDateTimeServiceImpl();

   @Test
   public void testGetCurrentDateTime() throws Exception
   {
      // given
      LocalDateTime localDateTime = LocalDateTime.now();

      // when
      LocalDateTime result = service.getCurrentDateTime();

      // then
      assertThat(result.getYear()).isEqualTo(localDateTime.getYear());
      assertThat(result.getMonth()).isEqualTo(localDateTime.getMonth());
      assertThat(result.getDayOfMonth()).isEqualTo(localDateTime.getDayOfMonth());
      assertThat(result.getHour()).isEqualTo(localDateTime.getHour());
      assertThat(result.getMinute()).isEqualTo(localDateTime.getMinute());
      assertThat(result.getSecond()).isEqualTo(localDateTime.getSecond());
   }

}