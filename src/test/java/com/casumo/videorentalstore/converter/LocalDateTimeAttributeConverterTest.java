package com.casumo.videorentalstore.converter;

import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class LocalDateTimeAttributeConverterTest
{
   private LocalDateTimeAttributeConverter converter = new LocalDateTimeAttributeConverter();

   @Test
   public void testConvertToDatabaseColumn() throws Exception
   {
      // given
      LocalDateTime localDateTime = LocalDateTime.of(2017, 9, 30, 22, 9, 10);

      // when
      Timestamp result = converter.convertToDatabaseColumn(localDateTime);

      // then
      assertThat(result.getNanos()).isEqualTo(localDateTime.getNano());
   }

   @Test
   public void testConvertToDatabaseColumnNullValue() throws Exception
   {
      // when
      Timestamp result = converter.convertToDatabaseColumn(null);

      // then
      assertThat(result).isNull();
   }

   @Test
   public void testConvertToEntityAttribute() throws Exception
   {
      // given
      Timestamp timestamp = Timestamp.valueOf("2017-9-30 22:13:16");

      // when
      LocalDateTime result = converter.convertToEntityAttribute(timestamp);

      // then
      assertThat(result.getNano()).isEqualTo(timestamp.getNanos());
   }

   @Test
   public void testConvertToEntityAttributeNullValue() throws Exception
   {
      // when
      LocalDateTime result = converter.convertToEntityAttribute(null);

      // then
      assertThat(result).isNull();
   }

}