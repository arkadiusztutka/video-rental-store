package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.FilmType;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class FilmDTOMapperTest
{
   private FilmDTOMapper mapper = new FilmDTOMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      Long id = 100L;
      String title = "title";
      FilmType filmType = FilmType.REGULAR;
      Film film = mock(Film.class, "film");
      given(film.getId()).willReturn(id);
      given(film.getTitle()).willReturn(title);
      given(film.getType()).willReturn(filmType);

      // when
      FilmDTO result = mapper.map(film);

      // then
      then(film).should().getId();
      then(film).should().getTitle();
      then(film).should().getType();

      assertThat(result).isNotNull();
      assertThat(result.getId()).isEqualTo(id);
      assertThat(result.getTitle()).isEqualTo(title);
      assertThat(result.getType()).isEqualTo(filmType.getTypeName());
   }

}