package com.casumo.videorentalstore.domain.film;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FilmRepositoryTest
{
   @Autowired
   private FilmRepository repository;

   @Test
   public void testFindAllWhenQueryEmpty()
   {
      // given
      FilmQuery query = new FilmQuery();
      FilmSpecification filmSpecification = new FilmSpecification(query);

      // when
      List<Film> results = repository.findAll(filmSpecification);

      // then
      assertThat(results).hasSize(4);
      List<Long> ids = results.stream().map(Film::getId).collect(Collectors.toList());
      assertThat(ids).containsExactly(1L, 2L, 3L, 4L);
   }

   @Test
   public void testFindAllByTitle()
   {
      // given
      FilmQuery query = new FilmQuery();
      query.setTitle(Optional.of("Out Of Africa"));
      FilmSpecification filmSpecification = new FilmSpecification(query);

      // when
      List<Film> results = repository.findAll(filmSpecification);

      // then
      assertThat(results).hasSize(1);
      List<Long> ids = results.stream().map(Film::getId).collect(Collectors.toList());
      assertThat(ids).containsExactly(4L);
   }

   @Test
   public void testFindAllByType()
   {
      // given
      FilmQuery query = new FilmQuery();
      query.setType(Optional.of(FilmType.NEW_RELEASE));
      FilmSpecification filmSpecification = new FilmSpecification(query);

      // when
      List<Film> results = repository.findAll(filmSpecification);

      // then
      assertThat(results).hasSize(1);
      List<Long> ids = results.stream().map(Film::getId).collect(Collectors.toList());
      assertThat(ids).containsExactly(1L);
   }

   @Test
   public void testFindAllByTypeAndTitle()
   {
      // given
      FilmQuery query = new FilmQuery();
      query.setType(Optional.of(FilmType.OLD));
      query.setTitle(Optional.of("Out Of Africa"));
      FilmSpecification filmSpecification = new FilmSpecification(query);

      // when
      List<Film> results = repository.findAll(filmSpecification);

      // then
      assertThat(results).hasSize(1);
      List<Long> ids = results.stream().map(Film::getId).collect(Collectors.toList());
      assertThat(ids).containsExactly(4L);
   }

   @Test
   public void testFindAllNotFound()
   {
      // given
      FilmQuery query = new FilmQuery();
      query.setType(Optional.of(FilmType.NEW_RELEASE));
      query.setTitle(Optional.of("Out Of Africa"));
      FilmSpecification filmSpecification = new FilmSpecification(query);

      // when
      List<Film> results = repository.findAll(filmSpecification);

      // then
      assertThat(results).isEmpty();
   }
}