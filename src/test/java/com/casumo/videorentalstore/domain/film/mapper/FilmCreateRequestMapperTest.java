package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.FilmCreateRequest;
import com.casumo.videorentalstore.domain.film.FilmType;
import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class FilmCreateRequestMapperTest
{
   private FilmCreateRequestMapper mapper = new FilmCreateRequestMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      String title = "title";
      FilmType type = FilmType.REGULAR;
      FilmCreateRequestDTO requestDTO = mock(FilmCreateRequestDTO.class, "requestDTO");
      given(requestDTO.getTitle()).willReturn(title);
      given(requestDTO.getType()).willReturn(type);

      // when
      FilmCreateRequest result = mapper.map(requestDTO);

      // then
      then(requestDTO).should().getTitle();
      then(requestDTO).should().getType();

      assertThat(result).isNotNull();
      assertThat(result.getTitle()).isEqualTo(title);
      assertThat(result.getType()).isEqualTo(type);
   }

}