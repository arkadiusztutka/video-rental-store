package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.FilmQuery;
import com.casumo.videorentalstore.domain.film.FilmType;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class FilmQueryMapperTest
{
   private FilmQueryMapper mapper = new FilmQueryMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      String title = "title";
      FilmType type = FilmType.REGULAR;
      FilmQueryDTO queryDTO = mock(FilmQueryDTO.class, "queryDTO");
      given(queryDTO.getTitle()).willReturn(Optional.of(title));
      given(queryDTO.getType()).willReturn(Optional.of(type));

      // when
      FilmQuery result = mapper.map(queryDTO);

      // then
      then(queryDTO).should().getTitle();
      then(queryDTO).should().getType();

      assertThat(result).isNotNull();
      assertThat(result.getTitle().isPresent()).isTrue();
      assertThat(result.getTitle().get()).isEqualTo(title);
      assertThat(result.getType().isPresent()).isTrue();
      assertThat(result.getType().get()).isEqualTo(type);
   }

}