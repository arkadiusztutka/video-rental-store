package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.FilmType;
import com.casumo.videorentalstore.domain.film.FilmUpdateRequest;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class FilmUpdateRequestMapperTest
{
   private FilmUpdateRequestMapper mapper = new FilmUpdateRequestMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      Long id = 100L;
      String title = "title";
      FilmType type = FilmType.REGULAR;
      FilmUpdateRequestDTO requestDTO = mock(FilmUpdateRequestDTO.class, "requestDTO");
      given(requestDTO.getId()).willReturn(id);
      given(requestDTO.getTitle()).willReturn(Optional.of(title));
      given(requestDTO.getType()).willReturn(Optional.of(type));

      // when
      FilmUpdateRequest result = mapper.map(requestDTO);

      // then
      then(requestDTO).should().getId();
      then(requestDTO).should().getTitle();
      then(requestDTO).should().getType();

      assertThat(result).isNotNull();
      assertThat(result.getId()).isEqualTo(id);
      assertThat(result.getTitle().isPresent()).isTrue();
      assertThat(result.getTitle().get()).isEqualTo(title);
      assertThat(result.getType().isPresent()).isTrue();
      assertThat(result.getType().get()).isEqualTo(type);
   }

}