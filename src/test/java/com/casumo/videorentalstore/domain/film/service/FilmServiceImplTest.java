package com.casumo.videorentalstore.domain.film.service;

import com.casumo.videorentalstore.domain.film.*;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class FilmServiceImplTest
{
   @Mock
   private FilmRepository filmRepository;

   @InjectMocks
   private FilmServiceImpl service;

   @Test
   public void testFind() throws Exception
   {
      // given
      Long id = 199L;
      Film film = mock(Film.class, "film");
      given(filmRepository.findById(id)).willReturn(Optional.of(film));

      // when
      Optional<Film> result = service.find(id);

      // then
      then(filmRepository).should().findById(eq(id));

      assertThat(result.isPresent()).isTrue();
      assertThat(result.get()).isSameAs(film);
   }

   @Test
   public void testCreate() throws Exception
   {
      // given
      Film film = mock(Film.class, "film");
      FilmCreateRequest request = mock(FilmCreateRequest.class, "request");
      given(request.create()).willReturn(film);
      given(filmRepository.save(film)).willReturn(film);

      // when
      Film result = service.create(request);

      // then
      then(request).should().create();
      then(filmRepository).should().save(same(film));

      assertThat(result).isSameAs(film);
   }

   @Test
   public void testUpdateWhenFilmExists() throws Exception
   {
      // given
      Long id = 100L;
      Film film = mock(Film.class, "film");
      FilmUpdateRequest request = mock(FilmUpdateRequest.class, "request");
      given(request.getId()).willReturn(id);
      given(filmRepository.findById(id)).willReturn(Optional.of(film));
      willDoNothing().given(request).update(film);
      given(filmRepository.save(film)).willReturn(film);

      // when
      Film result = service.update(request);

      // then
      then(request).should().getId();
      then(filmRepository).should().findById(eq(id));
      then(request).should().update(same(film));
      then(filmRepository).should().save(same(film));

      assertThat(result).isSameAs(film);
   }

   @Test
   public void testUpdateWhenFilmNotExists() throws Exception
   {
      // given
      Long id = 100L;
      FilmUpdateRequest request = mock(FilmUpdateRequest.class, "request");
      given(request.getId()).willReturn(id);
      given(filmRepository.findById(id)).willReturn(Optional.empty());

      // when
      Throwable thrown = catchThrowable(() -> service.update(request));

      // then
      then(request).should().getId();
      then(filmRepository).should().findById(eq(id));

      assertThat(thrown).isInstanceOf(ObjectNotFoundException.class);
   }

   @Test
   public void testSearch() throws Exception
   {
      // given
      FilmQuery query = mock(FilmQuery.class, "query");
      Film film = mock(Film.class, "film");
      given(filmRepository.findAll(any(Specification.class))).willReturn(List.of(film));

      // when
      List<Film> results = service.search(query);

      // then
      then(filmRepository).should().findAll(any(Specification.class));

      assertThat(results).containsExactly(film);
   }

   @Test
   public void testDeleteWhenSuccess()
   {
      // given
      Long filmId = 100L;
      Film film = mock(Film.class, "film");
      given(filmRepository.existsById(filmId)).willReturn(true);
      willDoNothing().given(filmRepository).deleteById(filmId);

      // when
      service.delete(filmId);

      // then
      then(filmRepository).should().existsById(eq(filmId));
      then(filmRepository).should().deleteById(eq(filmId));
   }

}