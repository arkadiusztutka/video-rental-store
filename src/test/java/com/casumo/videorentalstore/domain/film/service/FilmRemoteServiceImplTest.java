package com.casumo.videorentalstore.domain.film.service;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.FilmCreateRequest;
import com.casumo.videorentalstore.domain.film.FilmQuery;
import com.casumo.videorentalstore.domain.film.FilmUpdateRequest;
import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class FilmRemoteServiceImplTest
{
   @Mock
   private FilmService filmService;

   @Mock
   private Mapper<Film, FilmDTO> filmDTOMapper;

   @Mock
   private Mapper<FilmCreateRequestDTO, FilmCreateRequest> filmCreateRequestMapper;

   @Mock
   private Mapper<FilmUpdateRequestDTO, FilmUpdateRequest> filmUpdateRequestMapper;

   @Mock
   private Mapper<FilmQueryDTO, FilmQuery> filmQueryMapper;

   @InjectMocks
   private FilmRemoteServiceImpl service = new FilmRemoteServiceImpl(filmService, filmDTOMapper, filmCreateRequestMapper, filmUpdateRequestMapper,
           filmQueryMapper);
   @Test
   public void testCreate() throws Exception
   {
      // given
      FilmCreateRequestDTO requestDTO = mock(FilmCreateRequestDTO.class, "requestDTO");
      FilmCreateRequest request = mock(FilmCreateRequest.class, "request");
      Film film = mock(Film.class, "film");
      FilmDTO filmDTO = mock(FilmDTO.class, "filmDTO");
      given(filmCreateRequestMapper.map(requestDTO)).willReturn(request);
      given(filmService.create(request)).willReturn(film);
      given(filmDTOMapper.map(film)).willReturn(filmDTO);

      // when
      FilmDTO result = service.create(requestDTO);

      // then
      then(filmCreateRequestMapper).should().map(same(requestDTO));
      then(filmService).should().create(same(request));
      then(filmDTOMapper).should().map(same(film));

      assertThat(result).isSameAs(filmDTO);
   }

   @Test
   public void testUpdate() throws Exception
   {
      // given
      FilmUpdateRequestDTO requestDTO = mock(FilmUpdateRequestDTO.class, "requestDTO");
      FilmUpdateRequest request = mock(FilmUpdateRequest.class, "request");
      Film film = mock(Film.class, "film");
      FilmDTO filmDTO = mock(FilmDTO.class, "filmDTO");
      given(filmUpdateRequestMapper.map(requestDTO)).willReturn(request);
      given(filmService.update(request)).willReturn(film);
      given(filmDTOMapper.map(film)).willReturn(filmDTO);

      // when
      FilmDTO result = service.update(requestDTO);

      // then
      then(filmUpdateRequestMapper).should().map(same(requestDTO));
      then(filmService).should().update(same(request));
      then(filmDTOMapper).should().map(same(film));

      assertThat(result).isSameAs(filmDTO);
   }

   @Test
   public void testSearch() throws Exception
   {
      // given
      FilmQueryDTO queryDTO = mock(FilmQueryDTO.class, "queryDTO");
      FilmQuery filmQuery = mock(FilmQuery.class, "filmQuery");
      Film film = mock(Film.class, "film");
      List<Film> films = List.of(film);
      FilmDTO filmDTO = mock(FilmDTO.class, "filmDTO");
      given(filmQueryMapper.map(queryDTO)).willReturn(filmQuery);
      given(filmService.search(filmQuery)).willReturn(films);
      given(filmDTOMapper.map(films)).willReturn(List.of(filmDTO));

      // when
      List<FilmDTO> results = service.search(queryDTO);

      // then
      then(filmQueryMapper).should().map(same(queryDTO));
      then(filmService).should().search(same(filmQuery));
      then(filmDTOMapper).should().map(same(films));

      assertThat(results.isEmpty()).isFalse();
      assertThat(results).containsExactly(filmDTO);
   }

   @Test
   public void testDelete()
   {
      // given
      Long filmId = 100L;
      willDoNothing().given(filmService).delete(filmId);

      // when
      service.delete(filmId);

      // then
      then(filmService).should().delete(eq(filmId));
   }
}