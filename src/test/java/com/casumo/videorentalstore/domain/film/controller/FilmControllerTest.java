package com.casumo.videorentalstore.domain.film.controller;

import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;
import com.casumo.videorentalstore.domain.film.service.FilmRemoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class FilmControllerTest
{
   @Mock
   private FilmRemoteService filmRemoteService;

   @InjectMocks
   private FilmController filmController;

   @Test
   public void testCreate() throws Exception
   {
      // given
      FilmCreateRequestDTO requestDTO = mock(FilmCreateRequestDTO.class, "requestDTO");
      FilmDTO filmDTO = mock(FilmDTO.class, "filmDTO");
      given(filmRemoteService.create(requestDTO)).willReturn(filmDTO);

      // when
      FilmDTO result = filmController.create(requestDTO);

      // then
      then(filmRemoteService).should().create(same(requestDTO));
      assertThat(result).isSameAs(filmDTO);
   }

   @Test
   public void testUpdate() throws Exception
   {
      // given
      FilmUpdateRequestDTO requestDTO = mock(FilmUpdateRequestDTO.class, "requestDTO");
      FilmDTO filmDTO = mock(FilmDTO.class, "filmDTO");
      given(filmRemoteService.update(requestDTO)).willReturn(filmDTO);

      // when
      FilmDTO result = filmController.update(requestDTO);

      // then
      then(filmRemoteService).should().update(same(requestDTO));
      assertThat(result).isSameAs(filmDTO);
   }

   @Test
   public void testSearch() throws Exception
   {
      // given
      FilmQueryDTO queryDTO = mock(FilmQueryDTO.class, "queryDTO");
      FilmDTO filmDTO = mock(FilmDTO.class, "filmDTO");
      given(filmRemoteService.search(queryDTO)).willReturn(List.of(filmDTO));


      // when
      List<FilmDTO> results = filmController.search(queryDTO);

      // then
      then(filmRemoteService).should().search(same(queryDTO));
      assertThat(results).containsExactly(filmDTO);
   }

   @Test
   public void testDelete()
   {
      // given
      Long filmId = 100L;
      willDoNothing().given(filmRemoteService).delete(filmId);

      // when
      filmController.delete(filmId);

      // then
      then(filmRemoteService).should().delete(eq(filmId));
   }

}