package com.casumo.videorentalstore.domain.client.service;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.ClientRepository;
import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceImplTest
{
   @Mock
   private ClientRepository clientRepository;

   @InjectMocks
   private ClientServiceImpl service;

   @Test
   public void testFind() throws Exception
   {
      // given
      Long id = 100L;
      Client client = mock(Client.class, "client");
      given(clientRepository.findById(id)).willReturn(Optional.of(client));

      // when
      Optional<Client> result = service.find(id);

      // then
      then(clientRepository).should().findById(eq(id));
      assertThat(result.isPresent()).isTrue();
      assertThat(result.get()).isSameAs(client);
   }

   @Test
   public void testCreate() throws Exception
   {
      // given
      Client client = mock(Client.class, "client");
      ClientCreateRequest request = mock(ClientCreateRequest.class, "request");
      given(request.createUser()).willReturn(client);
      given(clientRepository.save(client)).willReturn(client);

      // when
      Client result = service.create(request);

      // then
      then(request).should().createUser();
      then(clientRepository).should().save(same(client));

      assertThat(result).isSameAs(client);
   }

   @Test
   public void testUpdateWhenClientExists() throws Exception
   {
      // given
      Long clientId = 100L;
      Client client = mock(Client.class, "client");
      ClientUpdateRequest request = mock(ClientUpdateRequest.class, "request");
      given(request.getId()).willReturn(clientId);
      given(clientRepository.findById(clientId)).willReturn(Optional.of(client));
      willDoNothing().given(request).update(client);
      given(clientRepository.save(client)).willReturn(client);

      // when
      Client result = service.update(request);

      // then
      then(request).should().getId();
      then(clientRepository).should().findById(eq(clientId));
      then(request).should().update(same(client));
      then(clientRepository).should().save(same(client));

      assertThat(result).isSameAs(client);
   }

   @Test
   public void testUpdateWhenClientNotExists() throws Exception
   {
      // given
      Long clientId = 100L;
      ClientUpdateRequest request = mock(ClientUpdateRequest.class, "request");
      given(request.getId()).willReturn(clientId);
      given(clientRepository.findById(clientId)).willReturn(Optional.empty());

      // when
      Throwable thrown = catchThrowable(() -> service.update(request));

      // then
      then(request).should().getId();
      then(clientRepository).should().findById(eq(clientId));

      assertThat(thrown).isInstanceOf(ObjectNotFoundException.class);
   }

}