package com.casumo.videorentalstore.domain.client.controller;

import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;
import com.casumo.videorentalstore.domain.bonuspointssummary.service.BonusPointsSummaryRemoteService;
import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import com.casumo.videorentalstore.domain.client.service.ClientRemoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ClientControllerTest
{
   @Mock
   private ClientRemoteService clientRemoteService;

   @Mock
   private BonusPointsSummaryRemoteService bonusPointsSummaryRemoteService;

   @InjectMocks
   private ClientController controller;

   @Test
   public void testCreate() throws Exception
   {
      // given
      ClientDTO clientDTO = mock(ClientDTO.class, "clientDTO");
      ClientCreateRequestDTO requestDTO = mock(ClientCreateRequestDTO.class, "requestDTO");
      given(clientRemoteService.create(requestDTO)).willReturn(clientDTO);

      // when
      ClientDTO result = controller.create(requestDTO);

      // then
      then(clientRemoteService).should().create(same(requestDTO));

      assertThat(result).isSameAs(clientDTO);
   }

   @Test
   public void testUpdate() throws Exception
   {
      // given
      ClientDTO clientDTO = mock(ClientDTO.class, "clientDTO");
      ClientUpdateRequestDTO requestDTO = mock(ClientUpdateRequestDTO.class, "requestDTO");
      given(clientRemoteService.update(requestDTO)).willReturn(clientDTO);

      // when
      ClientDTO result = controller.update(requestDTO);

      // then
      then(clientRemoteService).should().update(same(requestDTO));

      assertThat(result).isSameAs(clientDTO);
   }

   @Test
   public void testGetBonusPoints() throws Exception
   {
      // given
      Long clientId = 100L;
      BonusPointsSummaryDTO bonusPointsSummaryDTO = mock(BonusPointsSummaryDTO.class, "bonusPointsSummaryDTO");
      given(bonusPointsSummaryRemoteService.find(clientId)).willReturn(bonusPointsSummaryDTO);

      // when
      BonusPointsSummaryDTO result = controller.getBonusPoints(clientId);

      // then
      then(bonusPointsSummaryRemoteService).should().find(eq(clientId));
      assertThat(result).isSameAs(bonusPointsSummaryDTO);
   }

}