package com.casumo.videorentalstore.domain.client.mapper;

import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class ClientCreateRequestMapperTest
{
   private ClientCreateRequestMapper mapper = new ClientCreateRequestMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      String email = "email";
      String firstName = "firstName";
      String lastName = "lastName";
      ClientCreateRequestDTO requestDTO = mock(ClientCreateRequestDTO.class, "requestDTO0");
      given(requestDTO.getEmail()).willReturn(email);
      given(requestDTO.getFirstName()).willReturn(Optional.of(firstName));
      given(requestDTO.getLastName()).willReturn(Optional.of(lastName));

      // when
      ClientCreateRequest result = mapper.map(requestDTO);

      // then
      then(requestDTO).should().getEmail();
      then(requestDTO).should().getFirstName();
      then(requestDTO).should().getLastName();

      assertThat(result).isNotNull();
      assertThat(result.getEmail()).isEqualTo(email);
      assertThat(result.getFirstName().isPresent()).isTrue();
      assertThat(result.getFirstName().get()).isEqualTo(firstName);
      assertThat(result.getLastName().isPresent()).isTrue();
      assertThat(result.getLastName().get()).isEqualTo(lastName);
   }

}