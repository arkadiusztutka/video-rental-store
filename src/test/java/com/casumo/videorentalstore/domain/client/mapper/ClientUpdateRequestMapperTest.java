package com.casumo.videorentalstore.domain.client.mapper;

import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class ClientUpdateRequestMapperTest
{
   private ClientUpdateRequestMapper mapper = new ClientUpdateRequestMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      Long id = 100L;
      String email = "email";
      String firstName = "firstName";
      String lastName = "lastName";
      ClientUpdateRequestDTO requestDTO = mock(ClientUpdateRequestDTO.class, "clientUpdateRequestDTO");
      given(requestDTO.getId()).willReturn(id);
      given(requestDTO.getEmail()).willReturn(Optional.of(email));
      given(requestDTO.getFirstName()).willReturn(Optional.of(firstName));
      given(requestDTO.getLastName()).willReturn(Optional.of(lastName));

      // when
      ClientUpdateRequest result = mapper.map(requestDTO);

      // then
      then(requestDTO).should().getId();
      then(requestDTO).should().getEmail();
      then(requestDTO).should().getFirstName();
      then(requestDTO).should().getLastName();

      assertThat(result).isNotNull();
      assertThat(result.getId()).isEqualTo(id);
      assertThat(result.getEmail().isPresent()).isTrue();
      assertThat(result.getEmail().get()).isEqualTo(email);
      assertThat(result.getFirstName().isPresent()).isTrue();
      assertThat(result.getFirstName().get()).isEqualTo(firstName);
      assertThat(result.getLastName().isPresent()).isTrue();
      assertThat(result.getLastName().get()).isEqualTo(lastName);
   }

}