package com.casumo.videorentalstore.domain.client.mapper;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class ClientDTOMapperTest
{
   private ClientDTOMapper mapper = new ClientDTOMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      Long id = 100L;
      String firstName = "firstName";
      String lastName = "lastName";
      String email = "email";
      Client client = mock(Client.class, "client");
      given(client.getId()).willReturn(id);
      given(client.getFirstName()).willReturn(firstName);
      given(client.getLastName()).willReturn(lastName);
      given(client.getEmail()).willReturn(email);

      // when
      ClientDTO result = mapper.map(client);

      // then
      then(client).should().getId();
      then(client).should().getFirstName();
      then(client).should().getLastName();
      then(client).should().getEmail();

      assertThat(result).isNotNull();
      assertThat(result.getEmail()).isEqualTo(email);
      assertThat(result.getFirstName()).isEqualTo(firstName);
      assertThat(result.getLastName()).isEqualTo(lastName);
      assertThat(result.getId()).isEqualTo(id);
   }

}