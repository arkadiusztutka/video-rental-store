package com.casumo.videorentalstore.domain.client.service;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;
import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ClientRemoteServiceImplTest
{
   @Mock
   private Mapper<Client, ClientDTO> clientDTOMapper;

   @Mock
   private Mapper<ClientCreateRequestDTO, ClientCreateRequest> clientCreateRequestMapper;

   @Mock
   private Mapper<ClientUpdateRequestDTO, ClientUpdateRequest> clientUpdateRequestMapper;

   @Mock
   private ClientService clientService;

   @InjectMocks
   private ClientRemoteServiceImpl service = new ClientRemoteServiceImpl(clientDTOMapper, clientCreateRequestMapper, clientUpdateRequestMapper, clientService);

   @Test
   public void testCreate() throws Exception
   {
      // given
      ClientCreateRequestDTO requestDTO = mock(ClientCreateRequestDTO.class, "requestDTO");
      ClientCreateRequest request = mock(ClientCreateRequest.class, "request");
      Client client = mock(Client.class, "client");
      ClientDTO clientDTO = mock(ClientDTO.class, "clientDTO");
      given(clientCreateRequestMapper.map(requestDTO)).willReturn(request);
      given(clientService.create(request)).willReturn(client);
      given(clientDTOMapper.map(client)).willReturn(clientDTO);

      // when
      ClientDTO result = service.create(requestDTO);

      // then
      then(clientCreateRequestMapper).should().map(same(requestDTO));
      then(clientService).should().create(same(request));
      then(clientDTOMapper).should().map(same(client));

      assertThat(result).isSameAs(clientDTO);
   }

   @Test
   public void testUpdate() throws Exception
   {
      // given
      ClientUpdateRequestDTO requestDTO = mock(ClientUpdateRequestDTO.class, "requestDTO");
      ClientUpdateRequest request = mock(ClientUpdateRequest.class, "request");
      Client client = mock(Client.class, "client");
      ClientDTO clientDTO = mock(ClientDTO.class, "clientDTO");
      given(clientUpdateRequestMapper.map(requestDTO)).willReturn(request);
      given(clientService.update(request)).willReturn(client);
      given(clientDTOMapper.map(client)).willReturn(clientDTO);

      // when
      ClientDTO result = service.update(requestDTO);

      // then
      then(clientUpdateRequestMapper).should().map(same(requestDTO));
      then(clientService).should().update(same(request));
      then(clientDTOMapper).should().map(same(client));

      assertThat(result).isSameAs(clientDTO);
   }

}