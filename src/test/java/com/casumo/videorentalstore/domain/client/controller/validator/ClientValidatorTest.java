package com.casumo.videorentalstore.domain.client.controller.validator;

import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import org.junit.Test;
import org.springframework.validation.Errors;

import java.util.Optional;

import static com.casumo.videorentalstore.domain.client.controller.validator.ClientValidator.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class ClientValidatorTest
{
   private ClientValidator clientValidator = new ClientValidator();

   @Test
   public void testSupportsClientCreateRequestDTOClass() throws Exception
   {
      // when
      boolean result = clientValidator.supports(ClientCreateRequestDTO.class);

      // then
      assertThat(result).isTrue();
   }

   @Test
   public void testSupportsClientUpdateRequestDTOClass() throws Exception
   {
      // when
      boolean result = clientValidator.supports(ClientUpdateRequestDTO.class);

      // then
      assertThat(result).isTrue();
   }

   @Test
   public void testSupportsOtherClass() throws Exception
   {
      // when
      boolean result = clientValidator.supports(Integer.class);

      // then
      assertThat(result).isFalse();
   }

   @Test
   public void testValidateEmailIsNullInClientCreateRequestDTO() throws Exception
   {
      // given
      ClientCreateRequestDTO requestDTO = new ClientCreateRequestDTO();
      requestDTO.setEmail(null);
      Errors errors = mock(Errors.class, "errors");

      // when
      clientValidator.validate(requestDTO, errors);

      // then
      then(errors).should().rejectValue(eq(ClientCreateRequestDTO.PROPERTY_EMAIL), eq(ERROR_MISSING), eq(EMAIL_MISSING_MESSAGE));
   }

   @Test
   public void testValidateEmailValidInClientCreateRequestDTO() throws Exception
   {
      // given
      String email = "aa@wp.pl";
      ClientCreateRequestDTO requestDTO = new ClientCreateRequestDTO();
      requestDTO.setEmail(email);
      Errors errors = mock(Errors.class, "errors");

      // when
      clientValidator.validate(requestDTO, errors);

      // then
      then(errors).shouldHaveZeroInteractions();
   }

   @Test
   public void testValidateEmailInValidInClientCreateRequestDTO() throws Exception
   {
      // given
      String email = "aaa";
      ClientCreateRequestDTO requestDTO = new ClientCreateRequestDTO();
      requestDTO.setEmail(email);
      Errors errors = mock(Errors.class, "errors");

      // when
      clientValidator.validate(requestDTO, errors);

      // then
      then(errors).should().rejectValue(eq(ClientCreateRequestDTO.PROPERTY_EMAIL), eq(ERROR_INVALID), eq(EMAIL_INVALID_MESSAGE));
   }

   @Test
   public void testValidateEmailValidInClientUpdateRequestDTO() throws Exception
   {
      // given
      Long id = 100L;
      String email = "aa@wp.pl";
      ClientUpdateRequestDTO requestDTO = new ClientUpdateRequestDTO();
      requestDTO.setId(id);
      requestDTO.setEmail(Optional.of(email));
      Errors errors = mock(Errors.class, "errors");

      // when
      clientValidator.validate(requestDTO, errors);

      // then
      then(errors).shouldHaveZeroInteractions();
   }

   @Test
   public void testValidateEmailInValidInClientUpdateRequestDTO() throws Exception
   {
      // given
      Long id = 100L;
      String email = "aaa";
      ClientUpdateRequestDTO requestDTO = new ClientUpdateRequestDTO();
      requestDTO.setId(id);
      requestDTO.setEmail(Optional.of(email));
      Errors errors = mock(Errors.class, "errors");

      // when
      clientValidator.validate(requestDTO, errors);

      // then
      then(errors).should().rejectValue(eq(ClientUpdateRequestDTO.PROPERTY_EMAIL), eq(ERROR_INVALID), eq(EMAIL_INVALID_MESSAGE));
   }

   @Test
   public void testValidateIdIsNullInClientUpdateRequestDTO() throws Exception
   {
      // given
      ClientUpdateRequestDTO requestDTO = new ClientUpdateRequestDTO();
      requestDTO.setId(null);
      Errors errors = mock(Errors.class, "errors");

      // when
      clientValidator.validate(requestDTO, errors);

      // then
      then(errors).should().rejectValue(eq(ClientUpdateRequestDTO.PROPERTY_ID), eq(ERROR_MISSING), eq(ID_MISSING_MESSAGE));
   }

}