package com.casumo.videorentalstore.domain.bonuspointssummary.mapper;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;
import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class BonusPointsSummaryDTOMapperTest
{
   @InjectMocks
   private BonusPointsSummaryDTOMapper mapper;

   @Test
   public void testMap() throws Exception
   {
      // given
      int bonusPoints = 100;
      BonusPointsSummary bonusPointsSummary = mock(BonusPointsSummary.class, "bonusPointsSummary");
      given(bonusPointsSummary.getBonusPoints()).willReturn(bonusPoints);

      // when
      BonusPointsSummaryDTO result = mapper.map(bonusPointsSummary);

      // then
      then(bonusPointsSummary).should().getBonusPoints();
      assertThat(result).isNotNull();
      assertThat(result.getBonusPoints()).isEqualTo(bonusPoints);
   }

}