package com.casumo.videorentalstore.domain.bonuspointssummary.service;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;
import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummaryRepository;
import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.service.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class BonusPointsSummaryServiceImplTest
{
   @Mock
   private BonusPointsSummaryRepository bonusPointsSummaryRepository;

   @Mock
   private ClientService clientService;

   @InjectMocks
   private BonusPointsSummaryServiceImpl service;

   @Test
   public void testFind() throws Exception
   {
      // given
      Long clientId = 100L;
      Client client = mock(Client.class, "client");
      BonusPointsSummary bonusPointsSummary = mock(BonusPointsSummary.class, "bonusPointsSummary");
      given(clientService.find(clientId)).willReturn(Optional.of(client));
      given(bonusPointsSummaryRepository.findByClient(client)).willReturn(bonusPointsSummary);

      // when
      BonusPointsSummary result = service.find(clientId);

      // then
      then(clientService).should().find(eq(clientId));
      then(bonusPointsSummaryRepository).should().findByClient(same(client));

      assertThat(result).isSameAs(bonusPointsSummary);
   }

}