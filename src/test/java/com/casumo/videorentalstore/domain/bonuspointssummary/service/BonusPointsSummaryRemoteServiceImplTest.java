package com.casumo.videorentalstore.domain.bonuspointssummary.service;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;
import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class BonusPointsSummaryRemoteServiceImplTest
{
   @Mock
   private BonusPointsSummaryService bonusPointsSummaryService;

   @Mock
   private Mapper<BonusPointsSummary, BonusPointsSummaryDTO> bonusPointsSummaryDTOMapper;

   @InjectMocks
   private BonusPointsSummaryRemoteServiceImpl service;

   @Test
   public void testFind() throws Exception
   {
      // given
      Long clientId = 100L;
      BonusPointsSummary bonusPointsSummary = mock(BonusPointsSummary.class, "bonusPointsSummary");
      BonusPointsSummaryDTO bonusPointsSummaryDTO = mock(BonusPointsSummaryDTO.class, "bonusPointsSummaryDTO");
      given(bonusPointsSummaryService.find(clientId)).willReturn(bonusPointsSummary);
      given(bonusPointsSummaryDTOMapper.map(bonusPointsSummary)).willReturn(bonusPointsSummaryDTO);

      // when
      BonusPointsSummaryDTO result = service.find(clientId);

      // then
      then(bonusPointsSummaryService).should().find(eq(clientId));
      then(bonusPointsSummaryDTOMapper).should().map(same(bonusPointsSummary));

      assertThat(result).isSameAs(bonusPointsSummaryDTO);
   }

}