package com.casumo.videorentalstore.domain.bonuspointssummary;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BonusPointsSummaryRepositoryTest
{
   private static final Long CLIENT_ID = 1L;

   @Autowired
   private ClientRepository clientRepository;

   @Autowired
   private BonusPointsSummaryRepository repository;

   @Test
   public void testFindByClient() throws Exception
   {
      // given
      Optional<Client> client = clientRepository.findById(CLIENT_ID);

      // when
      BonusPointsSummary result = repository.findByClient(client.get());

      // then
      assertThat(result).isNotNull();
      assertThat(result.getBonusPoints()).isEqualTo(2);
      assertThat(result.getClient()).isEqualTo(client.get());
   }

}