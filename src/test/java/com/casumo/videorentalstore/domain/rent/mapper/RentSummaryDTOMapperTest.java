package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.rent.Rent;
import com.casumo.videorentalstore.domain.rent.RentSummary;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class RentSummaryDTOMapperTest
{
   private RentSummaryDTOMapper mapper = new RentSummaryDTOMapper();

   @Test
   public void testMap() throws Exception
   {
      // given
      Long rentId = 100L;
      Long filmId = 200L;
      BigDecimal price = BigDecimal.TEN;
      LocalDateTime localDateTime = LocalDateTime.of(2018, 4, 13, 18, 0 , 0);
      Rent rent = mock(Rent.class, "rent");
      Film film = mock(Film.class, "film");
      RentSummary rentSummary = mock(RentSummary.class, "rentSummary");
      given(rent.getId()).willReturn(rentId);
      given(rent.getFilm()).willReturn(film);
      given(film.getId()).willReturn(filmId);
      given(rentSummary.getPrice()).willReturn(price);
      given(rentSummary.getStartRentDateTime()).willReturn(localDateTime);
      given(rentSummary.getRents()).willReturn(List.of(rent));

      // when
      RentSummaryDTO result = mapper.map(rentSummary);

      // then
      then(rent).should().getId();
      then(rent).should().getFilm();
      then(film).should().getId();
      then(rentSummary).should().getPrice();
      then(rentSummary).should().getRents();
      then(rentSummary).should().getStartRentDateTime();

      assertThat(result).isNotNull();
      assertThat(result.getPrice()).isEqualTo(price);
      assertThat(result.getStartRentDateTime()).isEqualTo(localDateTime);
      assertThat(result.getRents()).hasSize(1);
      assertThat(result.getRents().get(0).getFilmId()).isEqualTo(filmId);
      assertThat(result.getRents().get(0).getRentId()).isEqualTo(rentId);
   }

}