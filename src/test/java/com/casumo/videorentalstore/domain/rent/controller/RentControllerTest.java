package com.casumo.videorentalstore.domain.rent.controller;

import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;
import com.casumo.videorentalstore.domain.rent.service.RentRemoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class RentControllerTest
{
   @Mock
   private RentRemoteService rentRemoteService;

   @InjectMocks
   private RentController controller;

   @Test
   public void testRent() throws Exception
   {
      // given
      RentCreateRequestDTO requestDTO = mock(RentCreateRequestDTO.class, "requestDTO");
      RentSummaryDTO rentSummaryDTO = mock(RentSummaryDTO.class, "rentSummaryDTO");
      given(rentRemoteService.rent(requestDTO)).willReturn(rentSummaryDTO);

      // when
      RentSummaryDTO result = controller.rent(requestDTO);

      // then
      then(rentRemoteService).should().rent(same(requestDTO));
      assertThat(result).isSameAs(rentSummaryDTO);
   }

   @Test
   public void testEndRent() throws Exception
   {
      // given
      RentEndRequestDTO requestDTO = mock(RentEndRequestDTO.class, "requestDTO");
      RentEndSummaryDTO rentEndSummaryDTO = mock(RentEndSummaryDTO.class, "rentEndSummaryDTO");
      given(rentRemoteService.endRent(requestDTO)).willReturn(rentEndSummaryDTO);

      // when
      RentEndSummaryDTO result = controller.endRent(requestDTO);

      // then
      then(rentRemoteService).should().endRent(same(requestDTO));
      assertThat(result).isSameAs(rentEndSummaryDTO);
   }

}