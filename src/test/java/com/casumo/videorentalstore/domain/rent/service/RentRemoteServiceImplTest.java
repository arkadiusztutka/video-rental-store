package com.casumo.videorentalstore.domain.rent.service;

import com.casumo.videorentalstore.domain.rent.RentCreateRequest;
import com.casumo.videorentalstore.domain.rent.RentEndRequest;
import com.casumo.videorentalstore.domain.rent.RentEndSummary;
import com.casumo.videorentalstore.domain.rent.RentSummary;
import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class RentRemoteServiceImplTest
{
   @Mock
   private RentService rentService;

   @Mock
   private Mapper<RentSummary, RentSummaryDTO> rentSummaryDTOMapper;

   @Mock
   private Mapper<RentEndSummary, RentEndSummaryDTO> rentEndSummaryDTOMapper;

   @Mock
   private Mapper<RentCreateRequestDTO, RentCreateRequest> rentCreateRequestMapper;

   @Mock
   private Mapper<RentEndRequestDTO, RentEndRequest> rentEndRequestMapper;

   @InjectMocks
   private RentRemoteServiceImpl service = new RentRemoteServiceImpl(rentService, rentSummaryDTOMapper, rentEndSummaryDTOMapper,
           rentCreateRequestMapper, rentEndRequestMapper);

   @Test
   public void testRent() throws Exception
   {
      // given
      RentCreateRequestDTO requestDTO = mock(RentCreateRequestDTO.class, "requestDTO");
      RentCreateRequest request = mock(RentCreateRequest.class, "request");
      RentSummaryDTO rentSummaryDTO = mock(RentSummaryDTO.class, "rentSummaryDTO");
      RentSummary rentSummary = mock(RentSummary.class, "rentSummary");
      given(rentCreateRequestMapper.map(requestDTO)).willReturn(request);
      given(rentService.rent(request)).willReturn(rentSummary);
      given(rentSummaryDTOMapper.map(rentSummary)).willReturn(rentSummaryDTO);

      // when
      RentSummaryDTO result = service.rent(requestDTO);

      // then
      then(rentCreateRequestMapper).should().map(same(requestDTO));
      then(rentService).should().rent(same(request));
      then(rentSummaryDTOMapper).should().map(same(rentSummary));

      assertThat(result).isSameAs(rentSummaryDTO);
   }

   @Test
   public void testEndRent() throws Exception
   {
      // given
      RentEndRequestDTO requestDTO = mock(RentEndRequestDTO.class, "requestDTO");
      RentEndRequest request = mock(RentEndRequest.class, "request");
      RentEndSummaryDTO rentEndSummaryDTO = mock(RentEndSummaryDTO.class, "rentEndSummaryDTO");
      RentEndSummary rentEndSummary = mock(RentEndSummary.class, "rentEndSummary");
      given(rentEndRequestMapper.map(requestDTO)).willReturn(request);
      given(rentService.endRent(request)).willReturn(rentEndSummary);
      given(rentEndSummaryDTOMapper.map(rentEndSummary)).willReturn(rentEndSummaryDTO);

      // when
      RentEndSummaryDTO result = service.endRent(requestDTO);

      // then
      then(rentEndRequestMapper).should().map(same(requestDTO));
      then(rentService).should().endRent(same(request));
      then(rentEndSummaryDTOMapper).should().map(same(rentEndSummary));

      assertThat(result).isSameAs(rentEndSummaryDTO);
   }

}