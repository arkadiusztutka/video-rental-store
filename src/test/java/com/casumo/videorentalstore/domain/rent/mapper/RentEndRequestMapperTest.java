package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.rent.Rent;
import com.casumo.videorentalstore.domain.rent.RentEndRequest;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.service.RentService;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class RentEndRequestMapperTest
{
   @Mock
   private RentService rentService;

   @InjectMocks
   private RentEndRequestMapper mapper;

   @Test
   public void testMap() throws Exception
   {
      // given
      Long rentId = 100L;
      RentEndRequestDTO requestDTO = mock(RentEndRequestDTO.class, "requestDTO");
      Rent rent = mock(Rent.class, "rent");
      given(requestDTO.getRents()).willReturn(List.of(rentId));
      given(rentService.find(rentId)).willReturn(Optional.of(rent));

      // when
      RentEndRequest result = mapper.map(requestDTO);

      // then
      then(requestDTO).should().getRents();
      then(rentService).should().find(eq(rentId));

      assertThat(result).isNotNull();
      assertThat(result.getRents()).containsExactly(rent);
   }

   @Test
   public void testMapWhenRentNotExists() throws Exception
   {
      // given
      Long rentId = 100L;
      RentEndRequestDTO requestDTO = mock(RentEndRequestDTO.class, "requestDTO");
      given(requestDTO.getRents()).willReturn(List.of(rentId));
      given(rentService.find(rentId)).willReturn(Optional.empty());

      // when
      Throwable thrown = catchThrowable(() -> mapper.map(requestDTO));

      // then
      then(requestDTO).should().getRents();
      then(rentService).should().find(eq(rentId));

      assertThat(thrown).isInstanceOf(ObjectNotFoundException.class);
   }

}