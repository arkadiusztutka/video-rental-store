package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.rent.RentEndSummary;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class RentEndSummaryDTOMapperTest
{
   private RentEndSummaryDTOMapper mapper = new RentEndSummaryDTOMapper();
   @Test
   public void testMap() throws Exception
   {
      // given
      BigDecimal lateCharge = BigDecimal.TEN;
      RentEndSummary rentEndSummary = mock(RentEndSummary.class, "rentEndSummary");
      given(rentEndSummary.getLateCharge()).willReturn(lateCharge);

      // when
      RentEndSummaryDTO result = mapper.map(rentEndSummary);

      // then
      then(rentEndSummary).should().getLateCharge();

      assertThat(result).isNotNull();
      assertThat(result.getLateCharge()).isEqualTo(lateCharge);
   }

}