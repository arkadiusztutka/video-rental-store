package com.casumo.videorentalstore.domain.rent.service;

import com.casumo.videorentalstore.datetime.CurrentDateTimeService;
import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.FilmType;
import com.casumo.videorentalstore.domain.rent.*;
import com.casumo.videorentalstore.domain.rent.RentCreateRequest.FilmRent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class RentServiceImplTest
{
   @Mock
   private CurrentDateTimeService currentDateTimeService;

   @Mock
   private RentRepository rentRepository;

   @InjectMocks
   private RentServiceImpl service;

   @Before
   public void setUp()
   {
      ReflectionTestUtils.setField(service, "premiumPrice", BigDecimal.valueOf(40));
      ReflectionTestUtils.setField(service, "basicPrice", BigDecimal.valueOf(30));
      ReflectionTestUtils.setField(service, "maxRentDaysForRegularFilm", 3);
      ReflectionTestUtils.setField(service, "maxRentDaysForOldFilm", 5);
      ReflectionTestUtils.setField(service, "bonusPointsForNewReleaseFilm", 2);
      ReflectionTestUtils.setField(service, "bonusPointsForStandardFilm", 1);
   }

   @Test
   public void testFind() throws Exception
   {
      // given
      Long id = 100L;
      Rent rent = mock(Rent.class, "rent");
      given(rentRepository.findById(id)).willReturn(Optional.of(rent));

      // when
      Optional<Rent> result = service.find(id);

      // then
      then(rentRepository).should().findById(eq(id));

      assertThat(result.isPresent()).isTrue();
      assertThat(result.get()).isSameAs(rent);
   }

   @Test
   public void testRent() throws Exception
   {
      // given
      BigDecimal price = BigDecimal.valueOf(320);
      LocalDateTime localDateTime = LocalDateTime.of(2018, 4, 10, 19, 30, 0);
      int daysOfRentForNewRelease = 5;
      int daysOfRentForRegular = 4;
      int daysOfRentForOld = 6;
      Rent rent = mock(Rent.class, "rent");
      List<Rent> rents = List.of(rent);
      Film newReleaseFilm = mock(Film.class, "newReleaseFilm");
      Film regularFilm = mock(Film.class, "regularFilm");
      Film oldFilm = mock(Film.class, "oldFilm");
      FilmRent newReleaseFilmRent = mock(FilmRent.class, "newReleaseFilmRent");
      FilmRent regularFilmRent = mock(FilmRent.class, "regularFilmRent");
      FilmRent oldFilmRent = mock(FilmRent.class, "oldFilmRent");
      RentCreateRequest request = mock(RentCreateRequest.class, "request");
      given(newReleaseFilm.getType()).willReturn(FilmType.NEW_RELEASE);
      given(regularFilm.getType()).willReturn(FilmType.REGULAR);
      given(oldFilm.getType()).willReturn(FilmType.OLD);
      given(newReleaseFilmRent.getRentDays()).willReturn(daysOfRentForNewRelease);
      given(newReleaseFilmRent.getFilm()).willReturn(newReleaseFilm);
      given(regularFilmRent.getRentDays()).willReturn(daysOfRentForRegular);
      given(regularFilmRent.getFilm()).willReturn(regularFilm);
      given(oldFilmRent.getRentDays()).willReturn(daysOfRentForOld);
      given(oldFilmRent.getFilm()).willReturn(oldFilm);
      given(currentDateTimeService.getCurrentDateTime()).willReturn(localDateTime);
      given(request.getFilmRents()).willReturn(List.of(newReleaseFilmRent, regularFilmRent, oldFilmRent));
      given(request.create(price, localDateTime)).willReturn(rents);
      given(rentRepository.saveAll(rents)).willReturn(rents);

      // when
      RentSummary result = service.rent(request);

      // then
      then(newReleaseFilm).should().getType();
      then(regularFilm).should().getType();
      then(oldFilm).should().getType();
      then(newReleaseFilmRent).should().getRentDays();
      then(newReleaseFilmRent).should().getFilm();
      then(regularFilmRent).should().getRentDays();
      then(regularFilmRent).should().getFilm();
      then(oldFilmRent).should().getRentDays();
      then(oldFilmRent).should().getFilm();
      then(currentDateTimeService).should().getCurrentDateTime();
      then(request).should().getFilmRents();
      then(request).should().create(eq(price), eq(localDateTime));
      then(rentRepository).should().saveAll(eq(rents));

      assertThat(result.getPrice()).isEqualTo(price);
      assertThat(result.getStartRentDateTime()).isEqualTo(localDateTime);
      assertThat(result.getRents()).containsExactly(rent);
   }

   @Test
   public void testEndRent() throws Exception
   {
      // given
      BigDecimal lateCharges = BigDecimal.valueOf(100);
      int daysOfRent = 6;
      LocalDateTime returnDateTime = LocalDateTime.of(2018, 4, 10, 19, 31, 0);
      LocalDateTime rentDateTime = LocalDateTime.of(2018, 4, 3, 19, 30, 0);
      RentEndRequest request = mock(RentEndRequest.class, "request");
      Rent rentWithNewRelease = mock(Rent.class, "rentWithNewRelease");
      Rent rentWithRegular = mock(Rent.class, "rentWithRegular");
      Rent rentWithOld = mock(Rent.class, "rentWithOld");
      List<Rent> rents = List.of(rentWithNewRelease, rentWithRegular, rentWithOld);
      Film newReleaseFilm = mock(Film.class, "newReleaseFilm");
      Film regularFilm = mock(Film.class, "regularFilm");
      Film oldFilm = mock(Film.class, "oldFilm");
      given(rentWithNewRelease.getFilm()).willReturn(newReleaseFilm);
      given(rentWithNewRelease.getRentDateTime()).willReturn(rentDateTime);
      given(rentWithNewRelease.getDays()).willReturn(daysOfRent);
      given(rentWithRegular.getFilm()).willReturn(regularFilm);
      given(rentWithRegular.getRentDateTime()).willReturn(rentDateTime);
      given(rentWithRegular.getDays()).willReturn(daysOfRent);
      given(rentWithOld.getFilm()).willReturn(oldFilm);
      given(rentWithOld.getRentDateTime()).willReturn(rentDateTime);
      given(rentWithOld.getDays()).willReturn(daysOfRent);
      given(newReleaseFilm.getType()).willReturn(FilmType.NEW_RELEASE);
      given(regularFilm.getType()).willReturn(FilmType.REGULAR);
      given(oldFilm.getType()).willReturn(FilmType.OLD);
      given(currentDateTimeService.getCurrentDateTime()).willReturn(returnDateTime);
      given(request.getRents()).willReturn(rents);
      willDoNothing().given(rentWithNewRelease).setReturnDateTime(returnDateTime);
      willDoNothing().given(rentWithNewRelease).setBonusPoints(2);
      willDoNothing().given(rentWithRegular).setReturnDateTime(returnDateTime);
      willDoNothing().given(rentWithRegular).setBonusPoints(1);
      willDoNothing().given(rentWithOld).setReturnDateTime(returnDateTime);
      willDoNothing().given(rentWithOld).setBonusPoints(1);
      given(rentRepository.saveAll(rents)).willReturn(rents);

      // when
      RentEndSummary result = service.endRent(request);

      // then
      then(currentDateTimeService).should().getCurrentDateTime();
      then(request).should().getRents();
      then(rentWithNewRelease).should(times(2)).getFilm();
      then(rentWithNewRelease).should().getRentDateTime();
      then(rentWithNewRelease).should().getDays();
      then(rentWithRegular).should(times(2)).getFilm();
      then(rentWithRegular).should().getRentDateTime();
      then(rentWithRegular).should().getDays();
      then(rentWithOld).should(times(2)).getFilm();
      then(rentWithOld).should().getRentDateTime();
      then(rentWithOld).should().getDays();
      then(newReleaseFilm).should(times(2)).getType();
      then(regularFilm).should(times(2)).getType();
      then(oldFilm).should(times(2)).getType();
      then(rentWithNewRelease).should().setReturnDateTime(eq(returnDateTime));
      then(rentWithNewRelease).should().setBonusPoints(eq(2));
      then(rentWithRegular).should().setReturnDateTime(eq(returnDateTime));
      then(rentWithRegular).should().setBonusPoints(eq(1));
      then(rentWithOld).should().setReturnDateTime(eq(returnDateTime));
      then(rentWithOld).should().setBonusPoints(eq(1));
      then(rentRepository).should().saveAll(eq(rents));

      assertThat(result.getLateCharge()).isEqualTo(lateCharges);
   }

}