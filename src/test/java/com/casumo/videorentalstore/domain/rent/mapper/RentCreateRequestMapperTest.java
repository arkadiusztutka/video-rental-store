package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.service.ClientService;
import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.service.FilmService;
import com.casumo.videorentalstore.domain.rent.RentCreateRequest;
import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO.FilmRentDTO;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class RentCreateRequestMapperTest
{
   @Mock
   private FilmService filmService;

   @Mock
   private ClientService clientService;

   @InjectMocks
   private RentCreateRequestMapper mapper;

   @Test
   public void testMap() throws Exception
   {
      // given
      Long clientId = 100L;
      Long filmId = 200L;
      int daysOfRent = 5;
      Client client = mock(Client.class, "client");
      Film film = mock(Film.class, "film");
      FilmRentDTO filmRentDTO = mock(FilmRentDTO.class, "filmRentDTO");
      RentCreateRequestDTO requestDTO = mock(RentCreateRequestDTO.class, "requestDTO");
      given(requestDTO.getClientId()).willReturn(clientId);
      given(requestDTO.getFilmRents()).willReturn(List.of(filmRentDTO));
      given(filmRentDTO.getFilmId()).willReturn(filmId);
      given(filmRentDTO.getRentDays()).willReturn(daysOfRent);
      given(clientService.find(clientId)).willReturn(Optional.of(client));
      given(filmService.find(filmId)).willReturn(Optional.of(film));

      // when
      RentCreateRequest result = mapper.map(requestDTO);

      // then
      then(requestDTO).should().getClientId();
      then(requestDTO).should().getFilmRents();
      then(filmRentDTO).should().getFilmId();
      then(filmRentDTO).should().getRentDays();
      then(clientService).should().find(eq(clientId));
      then(filmService).should().find(eq(filmId));

      assertThat(result.getClient()).isSameAs(client);
      assertThat(result.getFilmRents()).hasSize(1);
      assertThat(result.getFilmRents().get(0).getFilm()).isSameAs(film);
      assertThat(result.getFilmRents().get(0).getRentDays()).isSameAs(daysOfRent);
   }

   @Test
   public void testMapWhenClientNotExists() throws Exception
   {
      // given
      Long clientId = 100L;
      Long filmId = 200L;
      int daysOfRent = 5;
      Client client = mock(Client.class, "client");
      Film film = mock(Film.class, "film");
      FilmRentDTO filmRentDTO = mock(FilmRentDTO.class, "filmRentDTO");
      RentCreateRequestDTO requestDTO = mock(RentCreateRequestDTO.class, "requestDTO");
      given(requestDTO.getClientId()).willReturn(clientId);
      given(requestDTO.getFilmRents()).willReturn(List.of(filmRentDTO));
      given(filmRentDTO.getFilmId()).willReturn(filmId);
      given(filmRentDTO.getRentDays()).willReturn(daysOfRent);
      given(clientService.find(clientId)).willReturn(Optional.empty());
      given(filmService.find(filmId)).willReturn(Optional.of(film));

      // when
      Throwable thrown = catchThrowable(() -> mapper.map(requestDTO));

      // then
      then(requestDTO).should().getClientId();
      then(requestDTO).should().getFilmRents();
      then(filmRentDTO).should().getFilmId();
      then(filmRentDTO).should().getRentDays();
      then(clientService).should().find(eq(clientId));
      then(filmService).should().find(eq(filmId));

      assertThat(thrown).isInstanceOf(ObjectNotFoundException.class);
   }

   @Test
   public void testMapWhenFilmNotExists() throws Exception
   {
      // given
      Long clientId = 100L;
      Long filmId = 200L;
      int daysOfRent = 5;
      FilmRentDTO filmRentDTO = mock(FilmRentDTO.class, "filmRentDTO");
      RentCreateRequestDTO requestDTO = mock(RentCreateRequestDTO.class, "requestDTO");
      given(requestDTO.getFilmRents()).willReturn(List.of(filmRentDTO));
      given(filmRentDTO.getFilmId()).willReturn(filmId);
      given(filmService.find(filmId)).willReturn(Optional.empty());

      // when
      Throwable thrown = catchThrowable(() -> mapper.map(requestDTO));

      // then
      then(requestDTO).should().getFilmRents();
      then(filmRentDTO).should().getFilmId();
      then(filmService).should().find(eq(filmId));

      assertThat(thrown).isInstanceOf(ObjectNotFoundException.class);
   }

}