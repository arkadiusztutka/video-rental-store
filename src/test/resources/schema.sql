CREATE TABLE public.client (
  id bigint PRIMARY KEY,
  first_name VARCHAR,
  last_name VARCHAR,
  email VARCHAR not null CONSTRAINT client_email_unique UNIQUE
);

CREATE TABLE public.film (
  id bigint PRIMARY KEY,
  title VARCHAR not null,
  is_deleted boolean DEFAULT FALSE not null,
  type VARCHAR CONSTRAINT type_check CHECK (type = 'NEW_RELEASE' OR type = 'REGULAR' OR type='OLD')
);

CREATE TABLE public.rent (
  id bigint PRIMARY KEY,
  client_id bigint not null ,
  film_id bigint not null,
  rent_date_time timestamp not null,
  return_date_time timestamp,
  days integer not null,
  price numeric(5, 2),
  bonus_points integer,

  CONSTRAINT fk_rent_client_id FOREIGN KEY (client_id) REFERENCES public.client (id),
  CONSTRAINT fk_rent_film_id FOREIGN KEY (film_id) REFERENCES public.film (id)
);

CREATE VIEW public.bonus_points_summary AS SELECT rownum() as id, c.id as client_id, COALESCE(sum(r.bonus_points), 0) as
  bonus_points FROM public.client c LEFT JOIN public.rent r ON c.id = r.client_id GROUP BY c.id;