INSERT INTO public.client (id, email) VALUES (1, 'aa@wp.pl');

INSERT INTO public.film (id, title, type) VALUES (1, 'Matrix 11', 'NEW_RELEASE');
INSERT INTO public.film (id, title, type) VALUES (2, 'Spider Man', 'REGULAR');
INSERT INTO public.film (id, title, type) VALUES (3, 'Spider Man 2', 'REGULAR');
INSERT INTO public.film (id, title, type) VALUES (4, 'Out Of Africa', 'OLD');

INSERT INTO public.rent (id, client_id, film_id, rent_date_time, return_date_time, days, price, bonus_points) VALUES
  (1, 1, 1, '2018-02-25 17:23:03.247619', '2018-02-28 17:23:03.247619', 5, 120, 2);