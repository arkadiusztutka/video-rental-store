CREATE SEQUENCE public.client_id_seq START WITH 1 INCREMENT BY 1;
CREATE TABLE public.client (
  id bigint PRIMARY KEY DEFAULT nextval('client_id_seq'),
  first_name text,
  last_name text,
  email text not null CONSTRAINT client_email_unique UNIQUE
);

CREATE SEQUENCE public.film_id_seq START WITH 1 INCREMENT BY 1;
CREATE TABLE public.film (
  id bigint PRIMARY KEY DEFAULT nextval('film_id_seq'),
  title text not null,
  is_deleted boolean DEFAULT FALSE not null,
  type text CONSTRAINT type_check CHECK (type = 'NEW_RELEASE' OR type = 'REGULAR' OR type='OLD')
);

CREATE SEQUENCE public.rent_id_seq START WITH 1 INCREMENT BY 1;
CREATE TABLE public.rent (
  id bigint PRIMARY KEY DEFAULT nextval('rent_id_seq'),
  client_id bigint not null ,
  film_id bigint not null,
  rent_date_time timestamp not null,
  return_date_time timestamp,
  days integer not null,
  price numeric(5, 2),
  bonus_points integer,

  CONSTRAINT fk_rent_client_id FOREIGN KEY (client_id) REFERENCES public.client (id),
  CONSTRAINT fk_rent_film_id FOREIGN KEY (film_id) REFERENCES public.film (id)
);

CREATE VIEW public.bonus_points_summary AS
  SELECT row_number() OVER (ORDER BY c.id) as id, c.id as client_id, COALESCE(sum(r.bonus_points::integer), 0) as bonus_points FROM public.client c
  LEFT JOIN public.rent r ON c.id = r.client_id GROUP BY c.id;

INSERT INTO public.film (title, type) VALUES ('Matrix 11', 'NEW_RELEASE');
INSERT INTO public.film (title, type) VALUES ('Spider Man', 'REGULAR');
INSERT INTO public.film (title, type) VALUES ('Spider Man 2', 'REGULAR');
INSERT INTO public.film (title, type) VALUES ('Out Of Africa', 'OLD');
