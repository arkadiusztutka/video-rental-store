package com.casumo.videorentalstore.datetime;

import java.time.LocalDateTime;

public interface CurrentDateTimeService
{
   LocalDateTime getCurrentDateTime();
}
