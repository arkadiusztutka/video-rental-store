package com.casumo.videorentalstore.datetime;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class CurrentDateTimeServiceImpl implements CurrentDateTimeService
{
   @Override
   public LocalDateTime getCurrentDateTime()
   {
      return LocalDateTime.now();
   }
}
