package com.casumo.videorentalstore.domain.bonuspointssummary.service;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;
import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BonusPointsSummaryRemoteServiceImpl implements BonusPointsSummaryRemoteService
{
   private BonusPointsSummaryService bonusPointsSummaryService;
   private Mapper<BonusPointsSummary, BonusPointsSummaryDTO> bonusPointsSummaryDTOMapper;

   public BonusPointsSummaryRemoteServiceImpl(BonusPointsSummaryService bonusPointsSummaryService, Mapper<BonusPointsSummary, BonusPointsSummaryDTO> bonusPointsSummaryDTOMapper)
   {
      this.bonusPointsSummaryService = bonusPointsSummaryService;
      this.bonusPointsSummaryDTOMapper = bonusPointsSummaryDTOMapper;
   }

   @Override
   public BonusPointsSummaryDTO find(Long clientId)
   {
      BonusPointsSummary bonusPointsSummary = bonusPointsSummaryService.find(clientId);
      return bonusPointsSummaryDTOMapper.map(bonusPointsSummary);
   }
}
