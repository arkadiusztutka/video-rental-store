package com.casumo.videorentalstore.domain.bonuspointssummary;

import com.casumo.videorentalstore.domain.client.Client;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "bonus_points_summary")
@Immutable
public class BonusPointsSummary
{
   @Id
   private Integer id;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "client_id", updatable = false, nullable = false)
   private Client client;

   @Column(name = "bonus_points")
   private int bonusPoints = 0;
}
