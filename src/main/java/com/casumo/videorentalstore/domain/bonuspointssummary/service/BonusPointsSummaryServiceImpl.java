package com.casumo.videorentalstore.domain.bonuspointssummary.service;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;
import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummaryRepository;
import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.service.ClientService;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@Slf4j
public class BonusPointsSummaryServiceImpl implements BonusPointsSummaryService
{
   private BonusPointsSummaryRepository bonusPointsSummaryRepository;
   private ClientService clientService;

   public BonusPointsSummaryServiceImpl(BonusPointsSummaryRepository bonusPointsSummaryRepository, ClientService clientService)
   {
      this.bonusPointsSummaryRepository = bonusPointsSummaryRepository;
      this.clientService = clientService;
   }

   @Override
   public BonusPointsSummary find(Long clientId)
   {
      Optional<Client> clientOptional = clientService.find(clientId);
      if(!clientOptional.isPresent())
      {
         log.error("Client for given id {} not exists", clientId);
         throw new ObjectNotFoundException("There was no client with id " + clientId);
      }
      return bonusPointsSummaryRepository.findByClient(clientOptional.get());
   }
}
