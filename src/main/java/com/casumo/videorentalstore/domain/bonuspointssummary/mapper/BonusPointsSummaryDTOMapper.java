package com.casumo.videorentalstore.domain.bonuspointssummary.mapper;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;
import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class BonusPointsSummaryDTOMapper implements Mapper<BonusPointsSummary, BonusPointsSummaryDTO>
{
   @Override
   public BonusPointsSummaryDTO map(BonusPointsSummary bonusPointsSummary)
   {
      BonusPointsSummaryDTO bonusPointsSummaryDTO = new BonusPointsSummaryDTO();
      bonusPointsSummaryDTO.setBonusPoints(bonusPointsSummary.getBonusPoints());
      return bonusPointsSummaryDTO;
   }
}
