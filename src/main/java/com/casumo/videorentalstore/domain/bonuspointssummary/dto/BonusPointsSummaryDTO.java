package com.casumo.videorentalstore.domain.bonuspointssummary.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BonusPointsSummaryDTO
{
   private int bonusPoints;
}
