package com.casumo.videorentalstore.domain.bonuspointssummary;

import com.casumo.videorentalstore.domain.client.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface BonusPointsSummaryRepository extends CrudRepository<BonusPointsSummary, Long>
{
   /**
    * Finds information about bonus points for client
    * @param client
    *    {@link Client}
    * @return
    *    information about bonus points
    */
   BonusPointsSummary findByClient(Client client);
}
