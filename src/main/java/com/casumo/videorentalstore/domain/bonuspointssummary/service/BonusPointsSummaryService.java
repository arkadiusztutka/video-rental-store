package com.casumo.videorentalstore.domain.bonuspointssummary.service;

import com.casumo.videorentalstore.domain.bonuspointssummary.BonusPointsSummary;

public interface BonusPointsSummaryService
{
   /**
    * Finds information about bonus points
    * @param clientId
    *    given client id to get bonus points for
    * @return
    *    {@link BonusPointsSummary}
    */
   BonusPointsSummary find(Long clientId);
}
