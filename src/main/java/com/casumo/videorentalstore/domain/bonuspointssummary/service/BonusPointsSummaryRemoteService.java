package com.casumo.videorentalstore.domain.bonuspointssummary.service;

import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;

public interface BonusPointsSummaryRemoteService
{
   /**
    * Finds information about bonus points
    * @param clientId
    *    given client id to get bonus points for
    * @return
    *    {@link BonusPointsSummaryDTO}
    */
   BonusPointsSummaryDTO find(Long clientId);
}
