package com.casumo.videorentalstore.domain.rent;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RentSummary
{
   private List<Rent> rents = new ArrayList<>();
   private LocalDateTime startRentDateTime;
   private BigDecimal price;
}
