package com.casumo.videorentalstore.domain.rent;

import com.casumo.videorentalstore.converter.LocalDateTimeAttributeConverter;
import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.film.Film;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(schema = "public", name = "rent")
public class Rent implements Serializable
{
   @Id
   @GeneratedValue(generator = "rent_id_seq_gen")
   @SequenceGenerator(name="rent_id_seq_gen", schema = "public", sequenceName = "rent_id_seq", allocationSize = 1)
   private Long id;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "client_id", updatable = false, foreignKey = @ForeignKey(name = "fk_rent_client_id"))
   private Client client;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "film_id", updatable = false, foreignKey = @ForeignKey(name = "fk_rent_film_id"))
   private Film film;

   @Column(name = "rent_date_time")
   @Convert(converter = LocalDateTimeAttributeConverter.class)
   private LocalDateTime rentDateTime;

   @Column(name = "return_date_time")
   @Convert(converter = LocalDateTimeAttributeConverter.class)
   private LocalDateTime returnDateTime;

   @Column(name = "days")
   private int days;

   @Column(name = "price", precision = 5, scale = 2)
   private BigDecimal price;

   @Column(name = "bonus_points")
   private int bonusPoints;
}
