package com.casumo.videorentalstore.domain.rent.service;

import com.casumo.videorentalstore.domain.rent.*;

import java.util.Optional;

public interface RentService
{
   /**
    * Finds rents
    * @param id
    *    identifier of rent
    * @return
    *    return {@link Optional} with {@link Rent} or empty {@link Optional}
    */
   Optional<Rent> find(Long id);

   /**
    * Rents films
    * @param request
    *    information about renting
    * @return
    *    {@link RentSummary}
    */
   RentSummary rent(RentCreateRequest request);

   /**
    * End of renting films
    * @param request
    *    information about ending renting
    * @return
    *    {@link RentEndSummary}
    */
   RentEndSummary endRent(RentEndRequest request);
}
