package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.rent.Rent;
import com.casumo.videorentalstore.domain.rent.RentEndRequest;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.service.RentService;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import com.casumo.videorentalstore.mapper.Mapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class RentEndRequestMapper implements Mapper<RentEndRequestDTO, RentEndRequest>
{
   private RentService rentService;

   public RentEndRequestMapper(RentService rentService)
   {
      this.rentService = rentService;
   }

   @Override
   public RentEndRequest map(RentEndRequestDTO requestDTO)
   {
      RentEndRequest rentEndRequest = new RentEndRequest();
      List<Rent> rents = new ArrayList<>();
      for(Long rentId : requestDTO.getRents())
      {
         Optional<Rent> rentOptional = rentService.find(rentId);
         if(!rentOptional.isPresent())
         {
            log.error("Rent with given id {} not exitsts", rentId);
            throw new ObjectNotFoundException("Rent not found with id " + rentId);
         }
         rents.add(rentOptional.get());
      }
      rentEndRequest.setRents(rents);
      return rentEndRequest;
   }
}
