package com.casumo.videorentalstore.domain.rent.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RentEndRequestDTO
{
   private List<Long> rents = new ArrayList<>();
}
