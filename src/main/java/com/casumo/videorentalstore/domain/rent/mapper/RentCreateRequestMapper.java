package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.service.ClientService;
import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.service.FilmService;
import com.casumo.videorentalstore.domain.rent.RentCreateRequest;
import com.casumo.videorentalstore.domain.rent.RentCreateRequest.FilmRent;
import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO.FilmRentDTO;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import com.casumo.videorentalstore.mapper.Mapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class RentCreateRequestMapper implements Mapper<RentCreateRequestDTO, RentCreateRequest>
{
   private FilmService filmService;
   private ClientService clientService;

   public RentCreateRequestMapper(FilmService filmService, ClientService clientService)
   {
      this.filmService = filmService;
      this.clientService = clientService;
   }

   @Override
   public RentCreateRequest map(RentCreateRequestDTO rentCreateRequestDTO)
   {
      List<FilmRent> filmRents = new ArrayList<>();
      for(FilmRentDTO filmRentDTO : rentCreateRequestDTO.getFilmRents())
      {
         Long filmId = filmRentDTO.getFilmId();
         Optional<Film> filmOptional = filmService.find(filmId);
         if(!filmOptional.isPresent())
         {
            log.error("Film with given id {} not exists", filmId);
            throw new ObjectNotFoundException("Film not found for given id " + filmId);
         }
         filmRents.add(createFilmRent(filmOptional.get(), filmRentDTO.getRentDays()));
      }
      @NotNull Long clientId = rentCreateRequestDTO.getClientId();
      Optional<Client> clientOptional = clientService.find(clientId);
      if(!clientOptional.isPresent())
      {
         log.error("Client with given id {} not exists", clientId);
         throw new ObjectNotFoundException("Client not found for given id " + clientId);
      }
      RentCreateRequest request = new RentCreateRequest();
      request.setFilmRents(filmRents);
      request.setClient(clientOptional.get());
      return request;
   }

   private FilmRent createFilmRent(Film film, int rentDays)
   {
      FilmRent filmRent = new FilmRent();
      filmRent.setFilm(film);
      filmRent.setRentDays(rentDays);
      return filmRent;
   }


}
