package com.casumo.videorentalstore.domain.rent.service;

import com.casumo.videorentalstore.datetime.CurrentDateTimeService;
import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.FilmType;
import com.casumo.videorentalstore.domain.rent.*;
import com.casumo.videorentalstore.domain.rent.RentCreateRequest.FilmRent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.casumo.videorentalstore.domain.film.FilmType.NEW_RELEASE;
import static java.time.temporal.ChronoUnit.DAYS;

@Service
@Transactional
public class RentServiceImpl implements RentService
{
   @Value("${film.premium.price}")
   private BigDecimal premiumPrice;

   @Value("${film.basic.price}")
   private BigDecimal basicPrice;

   @Value("${film.regular.max-days}")
   private int maxRentDaysForRegularFilm;

   @Value("${film.old.max-days}")
   private int maxRentDaysForOldFilm;

   @Value("${film.premium.bonus-points}")
   private int bonusPointsForNewReleaseFilm;

   @Value("${film.standard.bonus-points}")
   private int bonusPointsForStandardFilm;

   private CurrentDateTimeService currentDateTimeService;
   private RentRepository rentRepository;

   public RentServiceImpl(CurrentDateTimeService currentDateTimeService, RentRepository rentRepository)
   {
      this.currentDateTimeService = currentDateTimeService;
      this.rentRepository = rentRepository;
   }

   @Override
   public Optional<Rent> find(Long id)
   {
      return rentRepository.findById(id);
   }

   @Override
   public RentSummary rent(RentCreateRequest request)
   {
      LocalDateTime rentDateTime = currentDateTimeService.getCurrentDateTime();
      BigDecimal price = calculateRentPrice(request.getFilmRents());
      List<Rent> rents = request.create(price, rentDateTime);
      rentRepository.saveAll(rents);
      RentSummary rentSummary = new RentSummary();
      rentSummary.setPrice(price);
      rentSummary.setRents(rents);
      rentSummary.setStartRentDateTime(rentDateTime);
      return rentSummary;
   }

   private BigDecimal calculateRentPrice(List<FilmRent> filmRents)
   {
      BigDecimal cost = BigDecimal.ZERO;
      for (FilmRent filmRent : filmRents)
      {
         FilmType type = filmRent.getFilm().getType();
         int rentDays = filmRent.getRentDays();
         if (type == NEW_RELEASE)
         {
            BigDecimal filmRentCost = premiumPrice.multiply(BigDecimal.valueOf(rentDays));
            cost = cost.add(filmRentCost);
            continue;
         }
         cost = cost.add(basicPrice);
         if(type == FilmType.REGULAR)
         {
            cost = addCostForRent(cost, rentDays, maxRentDaysForRegularFilm);
            continue;
         }
         cost = addCostForRent(cost, rentDays, maxRentDaysForOldFilm);
      }
      return cost;
   }

   private BigDecimal addCostForRent(BigDecimal cost, int rentDays, int maxRentDaysForFilm)
   {
      boolean isDaysOver = maxRentDaysForFilm > rentDays;
      if(isDaysOver)
      {
         return cost;
      }
      int daysOver = rentDays - maxRentDaysForFilm;
      return cost.add(basicPrice.multiply(BigDecimal.valueOf(daysOver)));
   }

   @Override
   public RentEndSummary endRent(RentEndRequest request)
   {
      LocalDateTime returnDateTime = currentDateTimeService.getCurrentDateTime();
      List<Rent> rents = request.getRents();
      BigDecimal lateCharge = calculateLateCharge(rents, returnDateTime);
      rents = rents.stream()
              .peek(r -> updateRentWithReturnDateAndBonusPoints(r, returnDateTime))
              .collect(Collectors.toList());
      rentRepository.saveAll(rents);
      return new RentEndSummary(lateCharge);
   }

   private void updateRentWithReturnDateAndBonusPoints(Rent rent, LocalDateTime returnDateTime)
   {
      rent.setReturnDateTime(returnDateTime);
      Film film = rent.getFilm();
      FilmType filmType = film.getType();
      rent.setBonusPoints(filmType == NEW_RELEASE ? bonusPointsForNewReleaseFilm : bonusPointsForStandardFilm);
   }

   private BigDecimal calculateLateCharge(List<Rent> rents, LocalDateTime returnDateTime)
   {
      BigDecimal lateCharge = BigDecimal.ZERO;
      for (Rent rent : rents)
      {
         Film film = rent.getFilm();
         FilmType filmType = film.getType();
         LocalDateTime rentDateTime = rent.getRentDateTime();
         int declaredDaysOfRent = rent.getDays();
         long rentDays = DAYS.between(rentDateTime, returnDateTime);
         if(rentDays <= declaredDaysOfRent)
         {
            continue;
         }
         BigDecimal daysToPay = BigDecimal.valueOf(rentDays - declaredDaysOfRent);
         BigDecimal charge = filmType == NEW_RELEASE ? premiumPrice.multiply(daysToPay) : basicPrice.multiply(daysToPay);
         lateCharge = lateCharge.add(charge);
      }
      return lateCharge;
   }
}
