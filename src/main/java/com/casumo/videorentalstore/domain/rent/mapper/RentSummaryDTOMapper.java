package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.rent.RentSummary;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO.RentDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RentSummaryDTOMapper implements Mapper<RentSummary, RentSummaryDTO>
{
   @Override
   public RentSummaryDTO map(RentSummary rentSummary)
   {
      RentSummaryDTO rentSummaryDTO = new RentSummaryDTO();
      rentSummaryDTO.setPrice(rentSummary.getPrice());
      rentSummaryDTO.setStartRentDateTime(rentSummary.getStartRentDateTime());
      List<RentDTO> rents = rentSummary.getRents().stream().map(r -> createRentDTO(r.getId(), r.getFilm().getId())).collect(Collectors.toList());
      rentSummaryDTO.setRents(rents);
      return rentSummaryDTO;
   }

   private RentDTO createRentDTO(Long rentId, Long filmId)
   {
      RentDTO rentDTO = new RentDTO();
      rentDTO.setFilmId(filmId);
      rentDTO.setRentId(rentId);
      return rentDTO;
   }
}
