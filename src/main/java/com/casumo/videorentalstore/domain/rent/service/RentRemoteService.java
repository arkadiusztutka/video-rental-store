package com.casumo.videorentalstore.domain.rent.service;

import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;

public interface RentRemoteService
{
   /**
    * Rents films
    * @param requestDTO
    *    information about renting
    * @return
    *    {@link RentSummaryDTO}
    */
   RentSummaryDTO rent(RentCreateRequestDTO requestDTO);

   /**
    * End of renting films
    * @param requestDTO
    *    information about ending renting
    * @return
    *    {@link RentEndSummaryDTO}
    */
   RentEndSummaryDTO endRent(RentEndRequestDTO requestDTO);
}
