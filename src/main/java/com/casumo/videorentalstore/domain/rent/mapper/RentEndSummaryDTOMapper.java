package com.casumo.videorentalstore.domain.rent.mapper;

import com.casumo.videorentalstore.domain.rent.RentEndSummary;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class RentEndSummaryDTOMapper implements Mapper<RentEndSummary, RentEndSummaryDTO>
{
   @Override
   public RentEndSummaryDTO map(RentEndSummary rentEndSummary)
   {
      RentEndSummaryDTO rentEndSummaryDTO = new RentEndSummaryDTO();
      rentEndSummaryDTO.setLateCharge(rentEndSummary.getLateCharge());
      return rentEndSummaryDTO;
   }
}
