package com.casumo.videorentalstore.domain.rent;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class RentEndSummary
{
   private BigDecimal lateCharge;

   public RentEndSummary(BigDecimal lateCharge)
   {
      this.lateCharge = lateCharge;
   }
}
