package com.casumo.videorentalstore.domain.rent.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class RentSummaryDTO
{
   private List<RentDTO> rents = new ArrayList<>();
   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
   private LocalDateTime startRentDateTime;
   private BigDecimal price;

   @Getter
   @Setter
   public static class RentDTO
   {
      private Long filmId;
      private Long rentId;
   }
}
