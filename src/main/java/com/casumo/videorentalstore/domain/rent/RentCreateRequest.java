package com.casumo.videorentalstore.domain.rent;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.client.Client;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class RentCreateRequest
{
   private List<FilmRent> filmRents = new ArrayList<>();
   private Client client;

   public List<Rent> create(BigDecimal price, LocalDateTime rentDateTime)
   {
      return filmRents.stream()
              .map(f -> createRent(f, price, rentDateTime))
              .collect(Collectors.toList());
   }

   private Rent createRent(FilmRent filmRent, BigDecimal price, LocalDateTime rentDateTime)
   {
      Rent rent = new Rent();
      rent.setClient(client);
      rent.setFilm(filmRent.getFilm());
      rent.setDays(filmRent.getRentDays());
      rent.setPrice(price);
      rent.setRentDateTime(rentDateTime);
      return rent;
   }

   @Getter
   @Setter
   public static class FilmRent
   {
      private Film film;
      private int rentDays;
   }
}
