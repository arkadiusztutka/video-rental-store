package com.casumo.videorentalstore.domain.rent.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class RentEndSummaryDTO
{
   private BigDecimal lateCharge = BigDecimal.ZERO;
}
