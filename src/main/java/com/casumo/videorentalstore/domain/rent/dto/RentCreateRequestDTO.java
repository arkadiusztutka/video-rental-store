package com.casumo.videorentalstore.domain.rent.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RentCreateRequestDTO
{
   @Valid
   private List<FilmRentDTO> filmRents = new ArrayList<>();

   @NotNull(message = "{validation.missing.clientId}")
   private Long clientId;

   @Getter
   @Setter
   public static class FilmRentDTO
   {
      @NotNull(message = "{validation.missing.filmId}")
      private Long filmId;

      @NotNull(message = "{validation.missing.rendDays}")
      private Integer rentDays;
   }
}
