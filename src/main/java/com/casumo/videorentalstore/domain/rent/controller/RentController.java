package com.casumo.videorentalstore.domain.rent.controller;

import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;
import com.casumo.videorentalstore.domain.rent.service.RentRemoteService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class RentController
{
   private RentRemoteService rentRemoteService;

   public RentController(RentRemoteService rentRemoteService)
   {
      this.rentRemoteService = rentRemoteService;
   }

   @PostMapping(value = "/rents")
   public RentSummaryDTO rent(@Valid @RequestBody RentCreateRequestDTO requestDTO)
   {
      return rentRemoteService.rent(requestDTO);
   }

   @PutMapping(value = "/rents/end")
   public RentEndSummaryDTO endRent(@RequestBody RentEndRequestDTO requestDTO)
   {
      return rentRemoteService.endRent(requestDTO);
   }
}
