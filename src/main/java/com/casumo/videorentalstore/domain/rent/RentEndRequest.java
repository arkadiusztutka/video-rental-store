package com.casumo.videorentalstore.domain.rent;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RentEndRequest
{
   private List<Rent> rents = new ArrayList<>();
}
