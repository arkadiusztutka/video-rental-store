package com.casumo.videorentalstore.domain.rent.service;

import com.casumo.videorentalstore.domain.rent.RentCreateRequest;
import com.casumo.videorentalstore.domain.rent.RentEndRequest;
import com.casumo.videorentalstore.domain.rent.RentEndSummary;
import com.casumo.videorentalstore.domain.rent.RentSummary;
import com.casumo.videorentalstore.domain.rent.dto.RentCreateRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndRequestDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentEndSummaryDTO;
import com.casumo.videorentalstore.domain.rent.dto.RentSummaryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RentRemoteServiceImpl implements RentRemoteService
{
   private RentService rentService;
   private Mapper<RentSummary, RentSummaryDTO> rentSummaryDTOMapper;
   private Mapper<RentEndSummary, RentEndSummaryDTO> rentEndSummaryDTOMapper;
   private Mapper<RentCreateRequestDTO, RentCreateRequest> rentCreateRequestMapper;
   private Mapper<RentEndRequestDTO, RentEndRequest> rentEndRequestMapper;

   public RentRemoteServiceImpl(RentService rentService, Mapper<RentSummary, RentSummaryDTO> rentSummaryDTOMapper,
                                Mapper<RentEndSummary, RentEndSummaryDTO> rentEndSummaryDTOMapper,
                                Mapper<RentCreateRequestDTO, RentCreateRequest> rentCreateRequestMapper, Mapper<RentEndRequestDTO, RentEndRequest> rentEndRequestMapper)
   {
      this.rentService = rentService;
      this.rentSummaryDTOMapper = rentSummaryDTOMapper;
      this.rentEndSummaryDTOMapper = rentEndSummaryDTOMapper;
      this.rentCreateRequestMapper = rentCreateRequestMapper;
      this.rentEndRequestMapper = rentEndRequestMapper;
   }

   @Override
   public RentSummaryDTO rent(RentCreateRequestDTO requestDTO)
   {
      RentCreateRequest request = rentCreateRequestMapper.map(requestDTO);
      RentSummary rentSummary = rentService.rent(request);
      return rentSummaryDTOMapper.map(rentSummary);
   }

   @Override
   public RentEndSummaryDTO endRent(RentEndRequestDTO requestDTO)
   {
      RentEndRequest request = rentEndRequestMapper.map(requestDTO);
      RentEndSummary rentEndSummary = rentService.endRent(request);
      return rentEndSummaryDTOMapper.map(rentEndSummary);
   }
}
