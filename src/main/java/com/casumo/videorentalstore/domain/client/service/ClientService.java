package com.casumo.videorentalstore.domain.client.service;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;

import java.util.Optional;

public interface ClientService
{
   /**
    * Finds client
    * @param id
    *    identifier of client
    * @return
    *    return {@link Optional} with {@link Client} or empty {@link Optional}
    */
   Optional<Client> find(Long id);

   /**
    * Creates client
    * @param request
    *    information about client to create
    * @return
    *    {@link Client}
    */
   Client create(ClientCreateRequest request);

   /**
    * Updates information about client
    * @param request
    *    information about client to update
    * @return
    *    {@link Client}
    */
   Client update(ClientUpdateRequest request);
}
