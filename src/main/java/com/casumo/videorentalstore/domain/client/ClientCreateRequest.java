package com.casumo.videorentalstore.domain.client;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class ClientCreateRequest
{
   private String email;
   private Optional<String> firstName = Optional.empty();
   private Optional<String> lastName = Optional.empty();

   public Client createUser()
   {
      Client client = new Client();
      client.setEmail(email);
      firstName.ifPresent(client::setFirstName);
      lastName.ifPresent(client::setLastName);
      return client;
   }
}
