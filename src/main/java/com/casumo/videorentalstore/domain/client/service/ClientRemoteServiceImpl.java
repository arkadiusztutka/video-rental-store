package com.casumo.videorentalstore.domain.client.service;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;
import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ClientRemoteServiceImpl implements ClientRemoteService
{
   private Mapper<Client, ClientDTO> clientDTOMapper;
   private Mapper<ClientCreateRequestDTO, ClientCreateRequest> clientCreateRequestMapper;
   private Mapper<ClientUpdateRequestDTO, ClientUpdateRequest> clientUpdateRequestMapper;
   private ClientService clientService;

   public ClientRemoteServiceImpl(Mapper<Client, ClientDTO> clientDTOMapper, Mapper<ClientCreateRequestDTO, ClientCreateRequest> clientCreateRequestMapper, Mapper<ClientUpdateRequestDTO, ClientUpdateRequest> clientUpdateRequestMapper, ClientService clientService)
   {
      this.clientDTOMapper = clientDTOMapper;
      this.clientCreateRequestMapper = clientCreateRequestMapper;
      this.clientUpdateRequestMapper = clientUpdateRequestMapper;
      this.clientService = clientService;
   }

   @Override
   public ClientDTO create(ClientCreateRequestDTO requestDTO)
   {
      ClientCreateRequest request = clientCreateRequestMapper.map(requestDTO);
      Client client = clientService.create(request);
      return clientDTOMapper.map(client);
   }

   @Override
   public ClientDTO update(ClientUpdateRequestDTO requestDTO)
   {
      ClientUpdateRequest request = clientUpdateRequestMapper.map(requestDTO);
      Client client = clientService.update(request);
      return clientDTOMapper.map(client);
   }
}
