package com.casumo.videorentalstore.domain.client.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Getter
@Setter
public class ClientCreateRequestDTO
{
   public static final String PROPERTY_EMAIL = "email";

   @NotNull
   private String email;
   private Optional<String> firstName = Optional.empty();
   private Optional<String> lastName = Optional.empty();
}
