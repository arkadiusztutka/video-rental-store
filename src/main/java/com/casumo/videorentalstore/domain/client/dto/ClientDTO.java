package com.casumo.videorentalstore.domain.client.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClientDTO
{
   private Long id;
   private String email;
   private String firstName;
   private String lastName;
}
