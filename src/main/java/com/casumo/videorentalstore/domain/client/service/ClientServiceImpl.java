package com.casumo.videorentalstore.domain.client.service;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.ClientRepository;
import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@Slf4j
public class ClientServiceImpl implements ClientService
{
   private ClientRepository clientRepository;

   public ClientServiceImpl(ClientRepository clientRepository)
   {
      this.clientRepository = clientRepository;
   }

   @Override
   public Optional<Client> find(Long id)
   {
      return clientRepository.findById(id);
   }

   @Override
   public Client create(ClientCreateRequest request)
   {
      Client client = request.createUser();
      return clientRepository.save(client);
   }

   @Override
   public Client update(ClientUpdateRequest request)
   {
      Long clientId = request.getId();
      Optional<Client> clientOptional = clientRepository.findById(clientId);
      if(!clientOptional.isPresent())
      {
         log.error("Client with given id {} not exists", clientId);
         throw new ObjectNotFoundException("Client not found for given id" + clientId);
      }
      Client client = clientOptional.get();
      request.update(client);
      return clientRepository.save(client);
   }
}
