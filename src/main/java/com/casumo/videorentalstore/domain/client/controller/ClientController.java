package com.casumo.videorentalstore.domain.client.controller;

import com.casumo.videorentalstore.domain.bonuspointssummary.dto.BonusPointsSummaryDTO;
import com.casumo.videorentalstore.domain.bonuspointssummary.service.BonusPointsSummaryRemoteService;
import com.casumo.videorentalstore.domain.client.controller.validator.ClientValidator;
import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import com.casumo.videorentalstore.domain.client.service.ClientRemoteService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ClientController
{
   private ClientRemoteService clientRemoteService;
   private BonusPointsSummaryRemoteService bonusPointsSummaryRemoteService;
   private ClientValidator clientValidator;

   public ClientController(ClientRemoteService clientRemoteService, BonusPointsSummaryRemoteService bonusPointsSummaryRemoteService, ClientValidator clientValidator)
   {
      this.clientRemoteService = clientRemoteService;
      this.bonusPointsSummaryRemoteService = bonusPointsSummaryRemoteService;
      this.clientValidator = clientValidator;
   }

   @InitBinder
   protected void initBinder(WebDataBinder binder)
   {
      binder.setValidator(clientValidator);
   }

   @PostMapping(value = "/clients", produces = MediaType.APPLICATION_JSON_VALUE)
   public ClientDTO create(@Valid @RequestBody ClientCreateRequestDTO requestDTO)
   {
      return clientRemoteService.create(requestDTO);
   }

   @PutMapping(value = "/clients/update", produces = MediaType.APPLICATION_JSON_VALUE)
   public ClientDTO update(@Valid @RequestBody ClientUpdateRequestDTO requestDTO)
   {
      return clientRemoteService.update(requestDTO);
   }

   @GetMapping(value = "/clients/{clientId}/bonus-points", produces = MediaType.APPLICATION_JSON_VALUE)
   public BonusPointsSummaryDTO getBonusPoints(@PathVariable Long clientId)
   {
      return bonusPointsSummaryRemoteService.find(clientId);
   }
}
