package com.casumo.videorentalstore.domain.client;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class ClientUpdateRequest
{
   private Long id;
   private Optional<String> email = Optional.empty();
   private Optional<String> firstName = Optional.empty();
   private Optional<String> lastName = Optional.empty();

   public void update(Client client)
   {
      email.ifPresent(client::setEmail);
      firstName.ifPresent(client::setFirstName);
      lastName.ifPresent(client::setLastName);
   }
}
