package com.casumo.videorentalstore.domain.client.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Getter
@Setter
public class ClientUpdateRequestDTO
{
   public static final String PROPERTY_EMAIL = "email";
   public static final String PROPERTY_ID = "id";

   @NotNull
   private Long id;
   private Optional<String> email = Optional.empty();
   private Optional<String> firstName = Optional.empty();
   private Optional<String> lastName = Optional.empty();

}
