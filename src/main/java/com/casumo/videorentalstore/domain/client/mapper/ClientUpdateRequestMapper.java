package com.casumo.videorentalstore.domain.client.mapper;

import com.casumo.videorentalstore.domain.client.ClientUpdateRequest;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class ClientUpdateRequestMapper implements Mapper<ClientUpdateRequestDTO, ClientUpdateRequest>
{
   @Override
   public ClientUpdateRequest map(ClientUpdateRequestDTO requestDTO)
   {
      ClientUpdateRequest request = new ClientUpdateRequest();
      request.setId(requestDTO.getId());
      request.setEmail(requestDTO.getEmail());
      request.setFirstName(requestDTO.getFirstName());
      request.setLastName(requestDTO.getLastName());
      return request;
   }
}
