package com.casumo.videorentalstore.domain.client.controller.validator;

import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

@Component
public class ClientValidator implements Validator
{
   private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
   private static final ResourceBundle RESOURCE_BUNDLE = PropertyResourceBundle.getBundle("validationMessages_en");
   static final String EMAIL_INVALID_MESSAGE = RESOURCE_BUNDLE.getString("validation.invalid.email");
   static final String EMAIL_MISSING_MESSAGE = RESOURCE_BUNDLE.getString("validation.missing.email");
   static final String ID_MISSING_MESSAGE = RESOURCE_BUNDLE.getString("validation.missing.id");
   static final String ERROR_INVALID = "invalid";
   static final String ERROR_MISSING = "missing";

   @Override
   public boolean supports(Class<?> aClass)
   {
      return ClientCreateRequestDTO.class.equals(aClass) || ClientUpdateRequestDTO.class.equals(aClass);
   }

   @Override
   public void validate(@Nullable Object o, Errors errors)
   {
      if(o instanceof ClientCreateRequestDTO)
      {
         validClientCreateRequestDTO((ClientCreateRequestDTO) o, errors);
         return;
      }
      if(o instanceof ClientUpdateRequestDTO)
      {
         validClientUpdateRequestDTO((ClientUpdateRequestDTO) o, errors);
      }
   }

   private void validClientCreateRequestDTO(ClientCreateRequestDTO requestDTO, Errors errors)
   {
      String email = requestDTO.getEmail();
      if(email == null)
      {
         errors.rejectValue(ClientCreateRequestDTO.PROPERTY_EMAIL, ERROR_MISSING, EMAIL_MISSING_MESSAGE);
         return;
      }
      if(!email.matches(EMAIL_PATTERN))
      {
         errors.rejectValue(ClientCreateRequestDTO.PROPERTY_EMAIL, ERROR_INVALID, EMAIL_INVALID_MESSAGE);
      }
   }

   private void validClientUpdateRequestDTO(ClientUpdateRequestDTO requestDTO, Errors errors)
   {
      if(requestDTO.getId() == null)
      {
         errors.rejectValue(ClientUpdateRequestDTO.PROPERTY_ID, ERROR_MISSING, ID_MISSING_MESSAGE);
         return;
      }
      Optional<String> emailOptional = requestDTO.getEmail();
      if(!emailOptional.isPresent())
      {
         return;
      }
      String email = emailOptional.get();
      if(!email.matches(EMAIL_PATTERN))
      {
         errors.rejectValue(ClientUpdateRequestDTO.PROPERTY_EMAIL, ERROR_INVALID, EMAIL_INVALID_MESSAGE);
      }
   }
}
