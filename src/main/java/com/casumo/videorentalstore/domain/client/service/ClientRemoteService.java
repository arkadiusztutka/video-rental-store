package com.casumo.videorentalstore.domain.client.service;

import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import com.casumo.videorentalstore.domain.client.dto.ClientUpdateRequestDTO;

public interface ClientRemoteService
{
   /**
    * Creates client
    * @param requestDTO
    *    information about client to create
    * @return
    *    {@link ClientDTO}
    */
   ClientDTO create(ClientCreateRequestDTO requestDTO);

   /**
    * Updates information about client
    * @param requestDTO
    *    information about client to update
    * @return
    *    {@link ClientDTO}
    */
   ClientDTO update(ClientUpdateRequestDTO requestDTO);
}
