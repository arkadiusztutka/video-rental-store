package com.casumo.videorentalstore.domain.client;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(schema = "public", name = "client")
public class Client implements Serializable
{
   @Id
   @GeneratedValue(generator = "client_id_seq_gen")
   @SequenceGenerator(name="client_id_seq_gen", schema = "public", sequenceName = "client_id_seq", allocationSize = 1)
   private Long id;

   @Column(name = "first_name", nullable = false)
   private String firstName;

   @Column(name = "last_name", nullable = false)
   private String lastName;

   @Column(name = "email", nullable = false)
   @NaturalId
   private String email;

}
