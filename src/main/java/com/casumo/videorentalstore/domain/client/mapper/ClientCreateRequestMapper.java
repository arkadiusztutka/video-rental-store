package com.casumo.videorentalstore.domain.client.mapper;

import com.casumo.videorentalstore.mapper.Mapper;
import com.casumo.videorentalstore.domain.client.ClientCreateRequest;
import com.casumo.videorentalstore.domain.client.dto.ClientCreateRequestDTO;
import org.springframework.stereotype.Component;

@Component
public class ClientCreateRequestMapper implements Mapper<ClientCreateRequestDTO, ClientCreateRequest>
{
   @Override
   public ClientCreateRequest map(ClientCreateRequestDTO clientCreateRequestDTO)
   {
      ClientCreateRequest request = new ClientCreateRequest();
      request.setEmail(clientCreateRequestDTO.getEmail());
      request.setFirstName(clientCreateRequestDTO.getFirstName());
      request.setLastName(clientCreateRequestDTO.getLastName());
      return request;
   }
}
