package com.casumo.videorentalstore.domain.client.mapper;

import com.casumo.videorentalstore.domain.client.Client;
import com.casumo.videorentalstore.domain.client.dto.ClientDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class ClientDTOMapper implements Mapper<Client, ClientDTO>
{
   @Override
   public ClientDTO map(Client client)
   {
      ClientDTO clientDTO = new ClientDTO();
      clientDTO.setId(client.getId());
      clientDTO.setEmail(client.getEmail());
      clientDTO.setFirstName(client.getFirstName());
      clientDTO.setLastName(client.getLastName());
      return clientDTO;
   }
}
