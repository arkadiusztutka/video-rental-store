package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.FilmCreateRequest;
import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class FilmCreateRequestMapper implements Mapper<FilmCreateRequestDTO, FilmCreateRequest>
{
   @Override
   public FilmCreateRequest map(FilmCreateRequestDTO filmCreateRequestDTO)
   {
      FilmCreateRequest request = new FilmCreateRequest();
      request.setTitle(filmCreateRequestDTO.getTitle());
      request.setType(filmCreateRequestDTO.getType());
      return request;
   }
}
