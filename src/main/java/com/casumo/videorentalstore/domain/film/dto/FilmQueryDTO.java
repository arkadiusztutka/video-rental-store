package com.casumo.videorentalstore.domain.film.dto;

import com.casumo.videorentalstore.domain.film.FilmType;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class FilmQueryDTO
{
   private Optional<String> title = Optional.empty();
   private Optional<FilmType> type = Optional.empty();
}
