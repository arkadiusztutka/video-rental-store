package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class FilmDTOMapper implements Mapper<Film, FilmDTO>
{
   @Override
   public FilmDTO map(Film film)
   {
      FilmDTO filmDTO = new FilmDTO();
      filmDTO.setId(film.getId());
      filmDTO.setTitle(film.getTitle());
      filmDTO.setType(film.getType().getTypeName());
      return filmDTO;
   }
}
