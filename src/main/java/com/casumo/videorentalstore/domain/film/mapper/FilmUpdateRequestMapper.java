package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.FilmUpdateRequest;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class FilmUpdateRequestMapper implements Mapper<FilmUpdateRequestDTO, FilmUpdateRequest>
{
   @Override
   public FilmUpdateRequest map(FilmUpdateRequestDTO filmUpdateRequestDTO)
   {
      FilmUpdateRequest request = new FilmUpdateRequest();
      request.setId(filmUpdateRequestDTO.getId());
      request.setTitle(filmUpdateRequestDTO.getTitle());
      request.setType(filmUpdateRequestDTO.getType());
      return request;
   }
}
