package com.casumo.videorentalstore.domain.film.mapper;

import com.casumo.videorentalstore.domain.film.FilmQuery;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class FilmQueryMapper implements Mapper<FilmQueryDTO, FilmQuery>
{
   @Override
   public FilmQuery map(FilmQueryDTO filmQueryDTO)
   {
      FilmQuery query = new FilmQuery();
      query.setTitle(filmQueryDTO.getTitle());
      query.setType(filmQueryDTO.getType());
      return query;
   }
}
