package com.casumo.videorentalstore.domain.film.service;

import com.casumo.videorentalstore.domain.film.*;
import com.casumo.videorentalstore.exception.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class FilmServiceImpl implements FilmService
{
   private FilmRepository filmRepository;

   public FilmServiceImpl(FilmRepository filmRepository)
   {
      this.filmRepository = filmRepository;
   }

   @Override
   public Optional<Film> find(Long id)
   {
      return filmRepository.findById(id);
   }

   @Override
   public Film create(FilmCreateRequest request)
   {
      Film film = request.create();
      filmRepository.save(film);
      return film;
   }

   @Override
   public Film update(FilmUpdateRequest request)
   {
      Long filmId = request.getId();
      Optional<Film> filmOptional = filmRepository.findById(filmId);
      if(!filmOptional.isPresent())
      {
         log.error("Film with given id {} not exists", filmId);
         throw new ObjectNotFoundException("Film not found for given id " + filmId);
      }
      Film film = filmOptional.get();
      request.update(film);
      filmRepository.save(film);
      return film;
   }

   @Override
   public List<Film> search(FilmQuery query)
   {
      FilmSpecification filmSpecification = new FilmSpecification(query);
      return filmRepository.findAll(filmSpecification);
   }

   @Override
   public void delete(Long filmId)
   {
      boolean notExists = !filmRepository.existsById(filmId);
      if(notExists)
      {
         log.error("Cannot delete film because given id {} not exists", filmId);
         throw new ObjectNotFoundException("Film not exist with given id " + filmId);
      }
      filmRepository.deleteById(filmId);
   }
}
