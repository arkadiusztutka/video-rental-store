package com.casumo.videorentalstore.domain.film;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class FilmUpdateRequest
{
   private Long id;
   private Optional<String> title = Optional.empty();
   private Optional<FilmType> type = Optional.empty();

   public void update(Film film)
   {
      title.ifPresent(film::setTitle);
      type.ifPresent(film::setType);
   }
}
