package com.casumo.videorentalstore.domain.film;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(schema = "public", name = "film")
@Where(clause = "is_deleted = 'FALSE'")
@SQLDelete(sql = "UPDATE public.film SET is_deleted = 'TRUE' WHERE id = ?")
public class Film
{
   public static final String TITLE_COLUMN = "title";
   public static final String TYPE_COLUMN = "type";

   @Id
   @GeneratedValue(generator = "film_id_seq_gen")
   @SequenceGenerator(name="film_id_seq_gen", schema = "public", sequenceName = "film_id_seq", allocationSize = 1)
   private Long id;

   @Column(name = "title", nullable = false)
   private String title;

   @Column(name = "type")
   @Enumerated(EnumType.STRING)
   private FilmType type;

   @Column(name = "is_deleted")
   private boolean deleted;
}
