package com.casumo.videorentalstore.domain.film.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FilmDTO
{
   private Long id;
   private String title;
   private String type;
}
