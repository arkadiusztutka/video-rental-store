package com.casumo.videorentalstore.domain.film.controller;

import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;
import com.casumo.videorentalstore.domain.film.service.FilmRemoteService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
public class FilmController
{
   private FilmRemoteService filmRemoteService;

   public FilmController(FilmRemoteService filmRemoteService)
   {
      this.filmRemoteService = filmRemoteService;
   }

   @PostMapping("/films")
   public FilmDTO create(@Valid @RequestBody FilmCreateRequestDTO requestDTO)
   {
      return filmRemoteService.create(requestDTO);
   }

   @PutMapping("/films/update")
   public FilmDTO update(@Valid @RequestBody FilmUpdateRequestDTO requestDTO)
   {
      return filmRemoteService.update(requestDTO);
   }

   @GetMapping("/films/search")
   public List<FilmDTO> search(FilmQueryDTO queryDTO)
   {
      return filmRemoteService.search(queryDTO);
   }

   @DeleteMapping("/films/{filmId}/delete")
   public void delete(@NotNull @PathVariable Long filmId)
   {
      filmRemoteService.delete(filmId);
   }
}
