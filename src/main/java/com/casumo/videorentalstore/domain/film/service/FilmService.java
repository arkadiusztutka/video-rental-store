package com.casumo.videorentalstore.domain.film.service;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.FilmCreateRequest;
import com.casumo.videorentalstore.domain.film.FilmQuery;
import com.casumo.videorentalstore.domain.film.FilmUpdateRequest;

import java.util.List;
import java.util.Optional;

public interface FilmService
{
   /**
    * Finds film
    * @param id
    *    identifier of film
    * @return
    *    return {@link Optional} with {@link Film} or empty {@link Optional}
    */
   Optional<Film> find(Long id);

   /**
    * Create films
    * @param request
    *    information about creation of film
    * @return
    *    {@link Film}
    */
   Film create(FilmCreateRequest request);

   /**
    * Updates films
    * @param request
    *    information how update film
    * @return
    *    {@link Film}
    */
   Film update(FilmUpdateRequest request);

   /**
    * Searches films
    * @param query
    *    query for search
    * @return
    *    {@link List} of {@link Film}
    */
   List<Film> search(FilmQuery query);

   /**
    * Deletes film from database
    * @param filmId
    *    identifier of film
    */
   void delete(Long filmId);
}
