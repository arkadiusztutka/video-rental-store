package com.casumo.videorentalstore.domain.film;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class FilmQuery
{
   private Optional<String> title = Optional.empty();
   private Optional<FilmType> type = Optional.empty();
}
