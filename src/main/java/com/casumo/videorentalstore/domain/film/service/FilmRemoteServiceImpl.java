package com.casumo.videorentalstore.domain.film.service;

import com.casumo.videorentalstore.domain.film.Film;
import com.casumo.videorentalstore.domain.film.FilmCreateRequest;
import com.casumo.videorentalstore.domain.film.FilmQuery;
import com.casumo.videorentalstore.domain.film.FilmUpdateRequest;
import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;
import com.casumo.videorentalstore.mapper.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FilmRemoteServiceImpl implements FilmRemoteService
{
   private FilmService filmService;
   private Mapper<Film, FilmDTO> filmDTOMapper;
   private Mapper<FilmCreateRequestDTO, FilmCreateRequest> filmCreateRequestMapper;
   private Mapper<FilmUpdateRequestDTO, FilmUpdateRequest> filmUpdateRequestMapper;
   private Mapper<FilmQueryDTO, FilmQuery> filmQueryMapper;

   public FilmRemoteServiceImpl(FilmService filmService, Mapper<Film, FilmDTO> filmDTOMapper, Mapper<FilmCreateRequestDTO,
           FilmCreateRequest> filmCreateRequestMapper, Mapper<FilmUpdateRequestDTO, FilmUpdateRequest> filmUpdateRequestMapper,
                                Mapper<FilmQueryDTO, FilmQuery> filmQueryMapper)
   {
      this.filmService = filmService;
      this.filmDTOMapper = filmDTOMapper;
      this.filmCreateRequestMapper = filmCreateRequestMapper;
      this.filmUpdateRequestMapper = filmUpdateRequestMapper;
      this.filmQueryMapper = filmQueryMapper;
   }


   @Override
   public FilmDTO create(FilmCreateRequestDTO requestDTO)
   {
      FilmCreateRequest request = filmCreateRequestMapper.map(requestDTO);
      Film film = filmService.create(request);
      return filmDTOMapper.map(film);
   }

   @Override
   public FilmDTO update(FilmUpdateRequestDTO requestDTO)
   {
      FilmUpdateRequest request = filmUpdateRequestMapper.map(requestDTO);
      Film film = filmService.update(request);
      return filmDTOMapper.map(film);
   }

   @Override
   public List<FilmDTO> search(FilmQueryDTO queryDTO)
   {
      FilmQuery query = filmQueryMapper.map(queryDTO);
      List<Film> films = filmService.search(query);
      return filmDTOMapper.map(films);
   }

   @Override
   public void delete(Long filmId)
   {
      filmService.delete(filmId);
   }
}
