package com.casumo.videorentalstore.domain.film.dto;

import com.casumo.videorentalstore.domain.film.FilmType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FilmCreateRequestDTO
{
   @NotNull(message = "{validation.missing.title}")
   private String title;

   @NotNull(message = "{validation.missing.type}")
   private FilmType type;
}
