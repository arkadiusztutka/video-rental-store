package com.casumo.videorentalstore.domain.film;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilmCreateRequest
{
   private String title;
   private FilmType type;

   public Film create()
   {
      Film film = new Film();
      film.setTitle(title);
      film.setType(type);
      return film;
   }
}
