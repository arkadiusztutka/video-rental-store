package com.casumo.videorentalstore.domain.film;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FilmSpecification implements Specification<Film>
{
   private FilmQuery filmQuery;

   public FilmSpecification(FilmQuery filmQuery)
   {
      this.filmQuery = filmQuery;
   }

   @Nullable
   @Override
   public Predicate toPredicate(Root<Film> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder)
   {
      Optional<String> titleOptional = filmQuery.getTitle();
      List<Predicate> predicates = new ArrayList<>();
      if(titleOptional.isPresent())
      {
         Predicate predicate = criteriaBuilder.equal(root.get(Film.TITLE_COLUMN), titleOptional.get());
         predicates.add(predicate);
      }
      Optional<FilmType> typeOptional = filmQuery.getType();
      if(typeOptional.isPresent())
      {
         Predicate predicate = criteriaBuilder.equal(root.get(Film.TYPE_COLUMN), typeOptional.get());
         predicates.add(predicate);
      }
      return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
   }
}
