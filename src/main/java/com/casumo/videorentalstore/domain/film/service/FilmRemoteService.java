package com.casumo.videorentalstore.domain.film.service;

import com.casumo.videorentalstore.domain.film.dto.FilmCreateRequestDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmQueryDTO;
import com.casumo.videorentalstore.domain.film.dto.FilmUpdateRequestDTO;

import java.util.List;

public interface FilmRemoteService
{
   /**
    * Create films
    * @param requestDTO
    *    information about creation of film
    * @return
    *    {@link FilmDTO}
    */
   FilmDTO create(FilmCreateRequestDTO requestDTO);

   /**
    * Updates films
    * @param requestDTO
    *    information how update film
    * @return
    *    {@link FilmDTO}
    */
   FilmDTO update(FilmUpdateRequestDTO requestDTO);

   /**
    * Searches films
    * @param queryDTO
    *    query for search
    * @return
    *    {@link List} of {@link FilmDTO}
    */
   List<FilmDTO> search(FilmQueryDTO queryDTO);

   /**
    * Deletes film from database
    * @param filmId
    *    identifier of film
    */
   void delete(Long filmId);
}
