package com.casumo.videorentalstore.domain.film.dto;

import com.casumo.videorentalstore.domain.film.FilmType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Getter
@Setter
public class FilmUpdateRequestDTO
{
   @NotNull(message = "{validation.missing.id}")
   private Long id;
   private Optional<String> title = Optional.empty();
   private Optional<FilmType> type = Optional.empty();
}
