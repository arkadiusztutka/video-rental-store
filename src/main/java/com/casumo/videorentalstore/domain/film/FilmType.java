package com.casumo.videorentalstore.domain.film;

public enum FilmType
{
   NEW_RELEASE("New release"),
   REGULAR("Regular film"),
   OLD("Old film");

   private String typeName;

   FilmType(String typeName)
   {
      this.typeName = typeName;
   }

   public String getTypeName()
   {
      return typeName;
   }
}
