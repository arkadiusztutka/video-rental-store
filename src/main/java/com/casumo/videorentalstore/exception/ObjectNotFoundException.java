package com.casumo.videorentalstore.exception;

/**
 * Thrown when there is no object retrieved from db
 */
public class ObjectNotFoundException extends RuntimeException
{
   public ObjectNotFoundException(String message)
   {
      super(message);
   }


}
