package com.casumo.videorentalstore.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler
{
   private static final ResourceBundle RESOURCE_BUNDLE = PropertyResourceBundle.getBundle("validationMessages_en");
   static final String WRONG_DATA_MESSAGE = RESOURCE_BUNDLE.getString("error.illegaldata.msg");
   static final String INTERNAL_ERROR_MESSAGE = RESOURCE_BUNDLE.getString("error.internal.msg");
   static final String UNIQUENESS_VALUE_ERROR_MESSAGE = RESOURCE_BUNDLE.getString("error.uniquenessValues");

   @ExceptionHandler(value = {BindException.class, ObjectNotFoundException.class, MethodArgumentNotValidException.class,
           IllegalArgumentException.class})
   @ResponseStatus(HttpStatus.BAD_REQUEST)
   public ErrorResponse handleWrongDataException(HttpServletRequest req, Exception ex)
   {
      log.error("Error with wrong data", ex);
      if(ex instanceof BindException)
      {
         BindException bindException = (BindException) ex;
         return ErrorResponse.create(HttpStatus.BAD_REQUEST, bindException.getFieldError().getDefaultMessage());
      }
      if(ex instanceof MethodArgumentNotValidException)
      {
         MethodArgumentNotValidException methodArgumentNotValidException = (MethodArgumentNotValidException) ex;
         FieldError fieldError = methodArgumentNotValidException.getBindingResult().getFieldError();
         return ErrorResponse.create(HttpStatus.BAD_REQUEST, fieldError.getDefaultMessage());
      }
      return ErrorResponse.create(HttpStatus.BAD_REQUEST, WRONG_DATA_MESSAGE);
   }

   @ExceptionHandler(value = {DataIntegrityViolationException.class})
   @ResponseStatus(HttpStatus.BAD_REQUEST)
   public ErrorResponse handleDuplicatesValueError(DataIntegrityViolationException ex)
   {
      log.error("Duplicates values error", ex);
      return ErrorResponse.create(HttpStatus.BAD_REQUEST, UNIQUENESS_VALUE_ERROR_MESSAGE);
   }

   @ExceptionHandler(value = { Exception.class })
   @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
   public ErrorResponse handleUnknownException(Exception ex) {
      log.error("Internal application error", ex);
      return ErrorResponse.create(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_ERROR_MESSAGE);
   }
}
