package com.casumo.videorentalstore.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class ErrorResponse
{
   private HttpStatus status;
   private String errorMessage;

   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
   private LocalDateTime dateTime;

   private ErrorResponse(HttpStatus status, String errorMessage)
   {
      this.errorMessage = errorMessage;
      this.status = status;
      this.dateTime = LocalDateTime.now();
   }

   public static ErrorResponse create(HttpStatus status, String errorMessage)
   {
      return new ErrorResponse(status, errorMessage);
   }
}
